```
                    ______                      __  __
                   /  __  \__ ____  ___  ____  / /_/ /_
                  /  / /  / / __  / __ \/ __ \/ / / __ \
                 /  /_/  / / __  / / / / /_/ / / / /_/ /
                /  .____/_/_/ /_/_/ /_/\____/_/_/_____/
               /  /
              /__/   for piano numerical simulation

```


Pianolib in an open source c++ toolbox for the numerical simulation
of the piano. It is distributed under the terms of GNU GPLv3.  

The models and numerical methods implemented are following the PhD works
of Guillaume Castera and [Juliette Chabassier](http://juliettechabassier.perso.math.cnrs.fr/public/).  

Pianolib is a research code which may (and probably does) still contains bugs. Do not hesitate to report them.  

Pianolib includes some code freely adapted from [Montjoie](https://www.math.u-bordeaux.fr/~durufle/codes.php),
a finite element solver developed by [Marc Duruflé](https://www.math.u-bordeaux.fr/~durufle/)
and distributed under GNU Lesser General Public License.



## External dependencies

Before anything, install some basic libraries.  
For Linux users:
```sh
sudo apt-get install make cmake gfortran g++ mpich bzip2
```
Or, for Mac users:
```sh
brew install cmake gfortran wget
```
Then, the bash script `install_external.sh` should fetch and compile the following dependencies automatically.  
In the main directory (pianolib/), run:
```sh
./install_external.sh
```

### Seldon

[Seldon](https://www.math.u-bordeaux.fr/~durufle/seldon/) is mandatory. It is the linear algebra library used by Pianolib.  

### Blas, CBlas and Lapack

Seldon uses Blas, CBlas and [Lapack](https://github.com/Reference-LAPACK/lapack/) to perform very fast algebra on matrices and vectors.

### Metis

[Metis](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview) can be used as an ordering method for the LU decomposition of the matrices. It is optional for pianolib.

### Arpack

[Arpack](https://bitbucket.org/chaoyang2013/arpack) can be used to compute eigenvalues and eigenvectors of large sparse matrices.


## Compiling and Running the different rules

You may want to edit the Makefile with your favorite compiler, and then just run:
```sh
make
```
The default examples piano.ini and shank.ini are set to write the output files in the folder `simulations` that you must create by yourself with `mkdir simulations` in the pianolib main directory.

### Shank

`make shank` to compile this specific rule, and `./shank.x examples/shank.ini` to run the code.  

This is designed to demonstrate the efficiency of the kinetic energy
quadratization procedure described in (m2an paper under review).
A flexible hammer shank coupled with a hammer head impacts a rigid obstacle. No string is simulated.

### Full piano (still under development)

`make piano` to compile this specific rule, and `./piano.x examples/piano.ini` to run the code.  

The current release allows the simulation of a flexible shank and hammer
impacting one single string with SAV quadratization procedures.