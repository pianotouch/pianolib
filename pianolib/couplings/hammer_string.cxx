/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

/** Things concerning the coupling between the hammer and the string(s)
 * 
 * 
 * 
 */

namespace pianolib
{


  void
  Piano
  ::ComputeRepartitionHammerString() // Todo make it sparse vector
  {
    repartition_hammer.Reallocate(pstring.shape); repartition_hammer.Zero();
    Real_wp coef = 0.0;

    unsigned ii;
    Real_wp x;
    for (unsigned ne = 0; ne < pstring.Nx-1; ++ne) // Loop on elements
    {
      for (unsigned i = 0; i < pstring.nb_base; ++i) // Loop on test functions
      {
        ii = pstring.dof_num_H1(ne,i);
        x = (ne + pstring.quad_points(i))*pstring.dx;
        coef = Repartition(x, shank.hammer.slope, shank.hammer.impact_point, 
                                    shank.hammer.width)*pstring.quad_weights(i);

        if(abs(coef) > 1e-12)
        {
          repartition_hammer(pstring.model->dimension*ii) += pstring.dx*coef;
        }
      }
    }
  }



  void
  Piano
  ::ComputeConstDiffMatrixHammerStringCoupling(srmatrix &M) const
  {
    if (!hammer_is_removed)
    {
      for (unsigned i = 0; i < pstring.shape; ++i)
      {
        M.AddInteraction(pstring.offset+i, shank.shape+7, -repartition_hammer(i));;
      }
    }
  }



  void
  Piano
  ::ComputeDiffMatrixHammerStringCoupling(srmatrix &M) const
  {
    Real_wp d_tilde = -0.5*(3.0*shank.xi_y - shank.xi_y_minus);

#ifndef SHANK
    d_tilde += DotProd(pstring.Q_tilde, repartition_hammer);
#endif

    const Real_wp HeadSAV = shank.hammer.G(d_tilde, shank.auxiliary_constant_head);
    const Real_wp phi2 = shank.hammer.Phi2(d_tilde);

    M.AddInteraction(shank.shape+7, shank.shape+8, HeadSAV);
    M.AddInteraction(shank.shape+8, shank.shape+6, HeadSAV);
    M.AddInteraction(shank.shape+7, shank.shape+6, -phi2);

#ifndef SHANK
    for (unsigned i = 0; i < pstring.shape; ++i)
    {
      M.AddInteraction(shank.shape+7, pstring.offset+i, phi2*repartition_hammer(i));
      M.AddInteraction(shank.shape+8, pstring.offset+i, -HeadSAV*repartition_hammer(i));
    }
#endif
  }



  void
  Piano
  ::CheckHammerStuff()
  {
    if ((!contact_start) && (shank.string_force > 0))
    {
      contact_start = true;
      start_time_contact = current_time;

      speed_contact_trans = shank.xi_y_dot;
      speed_contact_longi = shank.xi_x_dot;
    }
    else if (contact_start && (!contact_stop) && (shank.string_force == 0))
    {
      contact_stop = true;
      contact_duration = current_time - start_time_contact;
    }

#ifndef SHANK // Do not try to remove it if it is the only thing which is simulated
    if (remove_hammer && contact_stop && !hammer_is_removed)
    {
      if (abs(shank.hammer.d0 + shank.xi_y_plus) > remove_hammer_threshold)
      {
        // If the hammer is sufficiently far away from the string
        // it is no longer useful to compute it
        hammer_is_removed = true;

        // Resize the system
        shape = pstring.shape + 1 + pstring.model->nb_LM;
        pstring.offset = 0;

        // Reset the jacobian matrix
        ConstDiffMatrix.Reallocate(shape, shape);
        pstring.ComputeConstDiffMatrix(ConstDiffMatrix);
        ComputeConstDiffMatrixBridgeCoupling(ConstDiffMatrix);
      }
    }
#endif
  }



  Real_wp
  Piano
  ::ComputeDissipatedEnergyHammerStringCoupling() const
  {
    Real_wp val_plus = -shank.xi_y_dot_plus;
    Real_wp val_mid  = -shank.xi_y_dot;
    Real_wp d_tilde  = -0.5*(3.0*shank.xi_y - shank.xi_y_minus);
#ifndef SHANK
    val_plus += DotProd(pstring.Q_dot_plus, repartition_hammer);
    val_mid  += DotProd(pstring.Q_dot, repartition_hammer);
    d_tilde  += DotProd(pstring.Q_tilde, repartition_hammer);
#endif

    return -shank.hammer.Phi2(d_tilde)*square(0.5*(val_plus + val_mid));
  }
  


} // namespace