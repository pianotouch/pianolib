/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

/** Things concerning the coupling between the string(s) and the soundboard
 * 
 * 
 * 
 */

namespace pianolib
{

  void
  Piano
  ::ComputeConstDiffMatrixBridgeCoupling(srmatrix &M) const
  {
    for (unsigned k = 0; k < pstring.model->nb_LM; ++k)
    {
      M.AddInteraction(pstring.offset+pstring.shape-pstring.model->dimension+k, 
                        pstring.offset+pstring.shape+1+k, -1.0);
      M.AddInteraction(pstring.offset+pstring.shape+1+k, 
                        pstring.offset+pstring.shape-pstring.model->dimension+k, 1.0);
    }
  }



  void
  Piano
  ::ComputeDiffMatrixBridgeCoupling(srmatrix &M) const
  {}



} // namespace