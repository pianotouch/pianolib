/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{


  class StiffShank : public StiffLinearBeam
  {
    // **********************************
    // Linear stiff two dimensional shank
    // **********************************

    //! Timoshenko equations (unknowns: w, phi)
    /*
      rho A d^2 w/dt^2 - A G K d^2 w/dx^2 + A G K dphi/dx = S(x, t)
      rho I d^2 phi/dt^2 - E I d^2 phi/dx^2 - A G K dw/dx + A G K phi = 0
    */
  public:


    Real_wp rubber_slope, rubber_width, rubber_position; // Rubber parameters

    Real_wp teta_coef;


    //! Default constructor
    StiffShank() : StiffLinearBeam()
    {
      length = 0.13;
      section = 6.31868e-5;
      radius = sqrt(section/pi);
      density = 560;
      young_mod = 10.18e9;
      quad_mod = 6.38999e-9;
      shear_mod = 0.64e9;
      kappa = 0.85;

      A.Reallocate(dimension, dimension); A.Zero();

      teta_coef = density*length*(quad_mod + section*square(length)/3.0);

      rubber_slope = 2000;
      rubber_width = 0.01;
      rubber_position = 0.0165;

      dirichlet_L(0) = false;
      dirichlet_L(1) = false;
    }

    inline void
    InitEquation() override
    {
      M(0,0) = density*section;
      M(1,1) = density*quad_mod;

      R(0,0) = 2.0*density*section*dampR(0);
      R(1,1) = 2.0*density*quad_mod*dampR(2);

      H(0,0) = 2.0*tension*dampH(0);
      H(1,1) = 2.0*young_mod*quad_mod*dampH(2);

      if ((dampR(0) != 0.0) || (dampR(2) != 0.0)
              || (dampH(0) != 0.0) || (dampH(2) != 0.0))
      {
        damped = true;
      }

      A(0,0) = section*shear_mod*kappa;
      A(1,1) = young_mod*quad_mod;

      B(0,1) = -section*shear_mod*kappa;

      C(1,1) = section*shear_mod*kappa;

      teta_coef = density*length*(quad_mod + section*square(length)/3.0);
    }

    inline Real_wp
    ComputeIntegraleU(const Real_wp &theta) const
    {
      return 0.5*density*section*gravity*square(length)*sin(theta);
    }

    inline Real_wp
    ComputeLagrangian(const rvector &q, const Real_wp &theta_dot) const
    {
      return 0.5*density*section*q(0)*q(0)*theta_dot*theta_dot;
    }

    inline void
    ComputeEL_theta(const rvector &q, const rvector &q_dot,
                    const Real_wp &theta_dot, const Real_wp &theta_ddot,
                    Real_wp &res) const
    {
      res = density*section*(2.0*q(0)*q_dot(0)*theta_dot + q(0)*q(0)*theta_ddot);
    }

    inline void
    ComputeEL_q(const rvector &q, const Real_wp &theta_dot, rvector &res) const
    {
      res(0) = -density*section*q(0)*theta_dot*theta_dot;
      res(1) = 0.0;
    }

  };

} // namespace