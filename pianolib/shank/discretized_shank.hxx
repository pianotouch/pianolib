/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{

  class Shank;
  class StiffShank;

  class DiscretizedShank : public DiscretizedBeam
  {

  public:

    sym_srmatrix Mh;
    srmatrix Rh, Kh;
    rvector MhVh, Vh, Ihw, SAVvect;
    Real_wp teta_coef_h;

    // Rubber repartition vector
    rvector rubber_repartition_w, rubber_repartition_phi;
    Real_wp rubber_repartition_theta;

    unsigned shape;


    DiscretizedShank();

    void AssembleFemMatrix(const StiffShank &shank);

    void OutputFemMatrices(const std::string &file) const;

    rvector AssembleRepartition(const unsigned &component, const StiffShank &shank) const;

    Real_wp ComputeIntegraleRepartitionTheta(const StiffShank &shank) const;

    rvector
    AssemblePositionVector(const StiffShank &shank, const bool &use_H1=true) const;

    rvector
    AssembleIntegralVector(const StiffShank &shank, const bool &use_H1=true) const;


    rvector
    AssembleSAV(const rvector &Q_tilde, const rvector &dQ_tilde,
                  const Real_wp &T_tilde, const Real_wp &dT_tilde, const Real_wp &ddT_tilde,
                  const Real_wp &auxiliary_constant, const StiffShank &shank) const;

    rvector
    AssembleSAVGrad(const Real_wp &T_tilde, const StiffShank &shank) const;

    rvector
    AssembleSAVLagragian(const rvector &Q_tilde, const rvector &dQ_tilde,
                          const Real_wp &T_tilde,
                          const Real_wp &dT_tilde,
                          const Real_wp &ddT_tilde,
                          const StiffShank &shank) const;

    Real_wp
    ComputeAuxVarShank(const rvector &Q_tilde, const Real_wp &T_tilde,
                        const Real_wp &dT_tilde, const Real_wp &auxiliary_constant, 
                        const StiffShank &shank) const;

    Real_wp
    ComputeIntegraleLagrangian(const StiffShank &shank,
                                const rvector &V,
                                const Real_wp &dT_tilde) const;

  };

} // namespace