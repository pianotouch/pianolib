/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{


  //! Class for the shank of the hammer
  class Shank : public DiscretizedShank, public DataFileReader
  {
  public:

    StiffShank shank;
    HammerHead hammer;

    Real_wp theta_init; // Initial angle theta

    Real_wp dt;
    unsigned nb_iter, current_iter;
    Real_wp duration, current_time;

    Real_wp cfl_fraction;

    unsigned offset; // Position of the string variables in the global piano DiffMatrix

    // Unknowns w and phi
    rvector Q_plus, Q, Q_minus;
    rvector Q_dot_plus, Q_dot, Q_dot_minus;

    // Lagrange mulitplier
    Real_wp lambda_plus, lambda, lambda_minus;

    // Unknown theta
    Real_wp theta_plus, theta, theta_minus;
    Real_wp theta_dot_plus, theta_dot, theta_dot_minus;
    Real_wp theta_ddot_plus, theta_ddot, theta_ddot_minus;

    // Extrapolation (3Q - Q_minus)/2
    rvector Q_tilde, Q_dot_tilde;
    Real_wp theta_tilde, theta_dot_tilde, theta_ddot_tilde;

    // Position of the center of mass
    Real_wp xi_x_minus, xi_x, xi_x_plus;
    Real_wp xi_y_minus, xi_y, xi_y_plus;
    Real_wp xi_x_dot_minus, xi_x_dot, xi_x_dot_plus;
    Real_wp xi_y_dot_minus, xi_y_dot, xi_y_dot_plus;

    // Auxiliary variable for the hammer head
    Real_wp Zh_plus, Zh_minus;

    // Auxiliary variable for the shank
    Real_wp Z_plus, Z_minus;

    // Auxiliary constants
    Real_wp auxiliary_constant_shank;
    Real_wp auxiliary_constant_head;

    // Shank - Head coupling force
    Real_wp coupling_force_x, coupling_force_y;

    // Force of the hammer on the string
    Real_wp string_force;


    Vector<WriteOnTheGoWithBuffer> outputs_shank, outputs_shank_grad;
    WriteOnTheGoWithBuffer output_theta;
    WriteOnTheGoWithBuffer output_auxiliary;
    WriteOnTheGoWithBuffer output_head;

    Vector<rvector> interp_values, interp_values_plus;
    Vector<rvector> interp_values_grad, interp_values_grad_plus;


    // File names
    std::string file_base;
    const std::string file_output_shank = "Sismo_Shank";
    const std::string file_output_shank_grad = "Sismo_Shank_Grad";
    const std::string file_output_theta = "Sismo_Shank_Theta";
    const std::string file_output_aux_var = "Sismo_Shank_AuxVar";
    const std::string file_output_head = "Sismo_Hammer_Head";

    unsigned size_buffer;
    bool interp_grad;
    bool compute_energy;
    bool output_fem_matrices;
    bool output_on_ddls, output_on_nodes;
    bool output_aux_var;

    Real_wp dt_outputs;


    rvector normal_force;
    Real_wp source_amplitude, source_start_time, source_duration;


    Shank();

    void SetInputData(const std::string& keyword, const Vector<std::string>& param);

    void DisplayInit(std::ostream& out) const;
    void WriteJsonSummary(JsonManager* jsm) const;

    void Init();

    void ChooseTimeStepCFL(Real_wp &dt_piano);

    void ComputeSourceTerms();

    void UpdateValues();

    void ApplyBoundaryConditions(srmatrix &M) const;

    void InitBuffer();

    void ConstructTimeInterp();

    Vector<rvector> ExtractCompsFromQ(const rvector &X) const;

    void WriteSismos(const DiscretizedBeam &grid_interp_time);

    void ComputeConstDiffMatrix(srmatrix &M) const;

    void ComputeDiffMatrix(srmatrix &M) const;

    void OneStep(srmatrix &M, rvector &RHS);

    void ExtractNewSol(const rvector &SOL);

    void ComputeRightHandSide(rvector &RHS) const;

    Real_wp ComputeKineticEnergy() const;
    Real_wp ComputePotentialEnergy() const;
    Real_wp ComputeNonLinearEnergy() const;
    Real_wp ComputeDissipatedEnergy() const;


  };
  
} // namespace