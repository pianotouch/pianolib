/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{


  //! Default constructor
  Shank::Shank() : DiscretizedShank(), shank(), hammer()
  {

    duration = 0.02;
    dt = -1;
    cfl_fraction = 10;

    current_time = 0.0;
    current_iter = 0;

    auxiliary_constant_shank = 1.0;
    auxiliary_constant_head = 1.0;


    file_base = "./";
    interp_grad = false;
    compute_energy = false;
    output_on_ddls = false;
    output_on_nodes = false;
    output_fem_matrices = false;
    output_aux_var = false;
    dt_outputs = 1e-5;
    size_buffer = 100;

    source_duration = -1;

  }



  void
  Shank
  ::SetInputData(const std::string &keyword, const Vector<std::string> &param)
  {
    if (keyword == "Shank") // Physical params of the hammer shank
    {
      shank.length     = to_num<Real_wp>(param(0));
      shank.section    = to_num<Real_wp>(param(1));
      shank.density    = to_num<Real_wp>(param(2));
      shank.young_mod  = to_num<Real_wp>(param(3));
      shank.quad_mod   = to_num<Real_wp>(param(4));
      shank.shear_mod  = to_num<Real_wp>(param(5));
      shank.kappa      = to_num<Real_wp>(param(6));

      shank.dampR(0)   = to_num<Real_wp>(param(7));
      shank.dampR(1)   = to_num<Real_wp>(param(8));

      shank.dampH(0)   = to_num<Real_wp>(param(9));
      shank.dampH(1)   = to_num<Real_wp>(param(10));
    }

    // Keywords for the head
    else if (keyword == "MassHead")
    {
      hammer.mass = to_num<Real_wp>(param(0));
    }
    else if (keyword == "Width")
    {
      hammer.width = to_num<Real_wp>(param(0));
    }
    else if (keyword == "Slope")
    {
      hammer.slope = to_num<Real_wp>(param(0));
    }
    else if (keyword == "d0")
    {
      hammer.top_height = to_num<Real_wp>(param(0));
    }
    else if (keyword == "H")
    {
      hammer.CoM_height = to_num<Real_wp>(param(0));
    }
    else if (keyword == "StringHeight")
    {
      hammer.string_height = to_num<Real_wp>(param(0));
    }
    else if (keyword == "StrikePoint")
    {
      hammer.impact_point = to_num<Real_wp>(param(0));
    }

    else if (keyword == "ContactModel")
    {
      if (param(0) == "THREE_PARAMS")
      {
        if (param.GetM() <= 3)
        {
          std::cout << "In SetInputData of Hammer" << std::endl;
          std::cout << "HammerStiffness needs 4 parameters, for instance :" << std::endl;
          std::cout << "HammerStiffness = THREE_PARAMS p K R" << std::endl;
          std::cout << "Current parameters are : " << std::endl << param << std::endl;
          abort();
        }

        hammer.p = to_num<Real_wp>(param(1));
        hammer.K = to_num<Real_wp>(param(2));
        hammer.R = to_num<Real_wp>(param(3));
        hammer.contact_model = hammer.THREE_PARAMS;
      }
      else if (param(0) == "FIVE_PARAMS")
      {
        if (param.GetM() <= 5)
        {
          std::cout << "In SetInputData of Hammer" << std::endl;
          std::cout << "HammerStiffness needs 6 parameters, for instance :" << std::endl;
          std::cout << "HammerStiffness = FIVE_PARAMS A B C D damp" << std::endl;
          std::cout << "Current parameters are : " << std::endl << param << std::endl;
          abort();
        }

        hammer.A = to_num<Real_wp>(param(1));
        hammer.B = to_num<Real_wp>(param(2));
        hammer.C = to_num<Real_wp>(param(3));
        hammer.D = to_num<Real_wp>(param(4));
        hammer.damp = to_num<Real_wp>(param(5));
        hammer.contact_model = hammer.FIVE_PARAMS;
      }
    }

    else if (keyword == "InitialHammerAngle")
    {
      theta_init = to_num<Real_wp>(param(0))*pi/180.0;
    }

    else if (keyword == "FemOrderShank")
    {
      order = to_num<Real_wp>(param(0));
    }
    else if (keyword == "NxShank")
    {
      Nx = to_num<Real_wp>(param(0));
    }

    else if (keyword == "AuxiliaryConstantShank")
    {
      auxiliary_constant_shank = to_num<Real_wp>(param(0));
    }
    else if (keyword == "AuxiliaryConstantHead")
    {
      auxiliary_constant_head = to_num<Real_wp>(param(0));
    }


    else if (keyword == "SismosShank")
    {
      if (param.GetM() <= 1)
      {
        std::cout << "In SetInputData of MultiString" << std::endl;
        std::cout << "Sismos needs at least two parameter, for instance :" << std::endl;
        std::cout << "Sismos = REGULAR Nx_output" << std::endl;
        std::cout << "Sismos = POINTS output_points" << std::endl;
        std::cout << "Sismos = DDLS" << std::endl;
        std::cout << "Current parameters are : " << std::endl << param << std::endl;
        abort();
      }

      if (param(0) == "REGULAR")
      {
        // Interpolation on a regular grid with Nx_output points
        Nx_output = to_num<unsigned>(param(1));
      }
      else if (param(0) == "POINTS")
      {
        // Interpolation on the specified points
        for (unsigned j = 1; j < param.GetM(); ++j)
        {
          output_points.PushBack(to_num<Real_wp>(param(j)));
        }
      }
      else if (param(0) == "DDLS")
      {
        // No interpolation, outputs on all the Ddls of the mesh
        output_on_ddls = true;
      }
      else if (param(0) == "NODES")
      {
        // No interpolation, outputs on all the nodes of the mesh
        output_on_nodes = true;
      }
    }
    else if (keyword == "OutputFemMatricesShank")
    {
      if (param(0) == "YES")
      {
        output_fem_matrices = true;
      }
    }
    else if (keyword == "Source")
    {
      if (param(0) == "ROBOTRAN")
      {
        abort();
      }
      else if (param(0) == "YES")
      {
        source_amplitude = to_num<Real_wp>(param(1));
        source_start_time = to_num<Real_wp>(param(2));
        source_duration = to_num<Real_wp>(param(3));
      }
    }


  }



  void
  Shank
  ::DisplayInit(std::ostream &out) const
  {
    out << "" << std::endl;
    out << " *-----------------------------------*" << std::endl;
    out << " *         SHANK PARAMETERS          *" << std::endl;
    out << " *-----------------------------*-----*" << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Length (m)                  | " << shank.length << std::endl;
    out << " | Radius (m)                  | " << shank.radius << std::endl;
    out << " | Section (m²)                | " << shank.section << std::endl;
    out << " | Density (kg/m³)             | " << shank.density << std::endl;
    out << " | Young modulus (Pa)          | " << shank.young_mod << std::endl;
    out << " | Quadratic momentum (m⁴)     | " << shank.quad_mod << std::endl;
    out << " | Sheer modulus (Pa)          | " << shank.shear_mod << std::endl;
    out << " | Timoshenko coefficient      | " << shank.kappa << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Dry Damping (w)             | " << shank.dampR(0) << std::endl;
    out << " | Dry Damping (phi)           | " << shank.dampR(1) << std::endl;
    out << " | Viscous Damping (w)         | " << shank.dampH(0) << std::endl;
    out << " | Viscous Damping (phi)       | " << shank.dampH(1) << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << "" << std::endl;
    out << " *-----------------------------------*" << std::endl;
    out << " *         HEAD PARAMETERS           *" << std::endl;
    out << " *-----------------------------*-----*" << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Mass (kg)                   | " << hammer.mass << std::endl;
    out << " | d0 (m)                      | " << hammer.top_height << std::endl;
    out << " | H (m)                       | " << hammer.CoM_height << std::endl;
    out << " | String height (m)           | " << hammer.string_height << std::endl;
    out << " | Strike point ()             | " << hammer.impact_point << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    if (hammer.contact_model == hammer.THREE_PARAMS)
    {
      out << " | Contact Model               | " << "THREE_PARAMS" << std::endl;
      out << " | p                           | " << hammer.p << std::endl;
      out << " | K                           | " << hammer.K << std::endl;
      out << " | R                           | " << hammer.R << std::endl;
    }
    else if (hammer.contact_model == hammer.FIVE_PARAMS)
    {
      out << " | Contact Model               | " << "FIVE_PARAMS" << std::endl;
      out << " | A                           | " << hammer.A << std::endl;
      out << " | B                           | " << hammer.B << std::endl;
      out << " | C                           | " << hammer.C << std::endl;
      out << " | D                           | " << hammer.D << std::endl;
      out << " | Damping coef                | " << hammer.damp << std::endl;
    }
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << "" << std::endl;
    out << " *------------------------------------*" << std::endl;
    out << " *        SPACE DISCRETISATION        *" << std::endl;
    out << " *-----------------------------*------*" << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Nx                          | " << Nx << std::endl;
    out << " | Order                       | " << order << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << "" << std::endl;
    out << " *------------------------------------*" << std::endl;
    out << " *         TIME DISCRETISATION        *" << std::endl;
    out << " *-----------------------------*------*" << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Scheme                      | " << "Scalar Lagrangian Quadratization" << std::endl;
    out << " | Head auxiliary constant     | " << auxiliary_constant_head << std::endl;
    out << " | Shank auxiliary constant    | " << auxiliary_constant_shank << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << "" << std::endl;
    out << "" << std::endl;
  }



  void
  Shank
  ::WriteJsonSummary(JsonManager* jsm) const
  {
    JsonCategory* js_shank = jsm->AddCategory("Shank");
    js_shank->AddMember("Length",                  to_str(shank.length));
    js_shank->AddMember("Radius",                  to_str(shank.radius));
    js_shank->AddMember("Section",                 to_str(shank.section));
    js_shank->AddMember("Density",                 to_str(shank.density));
    js_shank->AddMember("Young modulus",           to_str(shank.young_mod));
    js_shank->AddMember("Quadratic momentum",      to_str(shank.quad_mod));
    js_shank->AddMember("Sheer modulus",           to_str(shank.shear_mod));
    js_shank->AddMember("Timoshenko coefficient",  to_str(shank.kappa));
    js_shank->AddMember("Dry Damping Flexion",     to_str(shank.dampR(0)));
    js_shank->AddMember("Dry Damping Shear",       to_str(shank.dampR(1)));
    js_shank->AddMember("Viscous Damping Flexion", to_str(shank.dampH(0)));
    js_shank->AddMember("Viscous Damping Shear",   to_str(shank.dampH(1)));

    js_shank->AddMember("Nx",                      to_str(Nx));
    js_shank->AddMember("Order",                   to_str(order));
    js_shank->AddMember("Auxiliary constant",      to_str(auxiliary_constant_shank));


    JsonCategory* js_hammer = jsm->AddCategory("Hammer");
    js_hammer->AddMember("Mass",                   to_str(hammer.mass));
    js_hammer->AddMember("d0",                     to_str(hammer.top_height));
    js_hammer->AddMember("H",                      to_str(hammer.CoM_height));
    js_hammer->AddMember("String height",          to_str(hammer.string_height));
    js_hammer->AddMember("Strike point",           to_str(hammer.impact_point));

    JsonSubCategory* js_contact = js_hammer->AddSubCategory("Constact");
    if (hammer.contact_model == hammer.THREE_PARAMS)
    {
      js_contact->AddMember("Model",               "\"THREE_PARAMS\"");
      js_contact->AddMember("p",                   to_str(hammer.p));
      js_contact->AddMember("K",                   to_str(hammer.K));
      js_contact->AddMember("R",                   to_str(hammer.R));
    }
    else if (hammer.contact_model == hammer.FIVE_PARAMS)
    {
      js_contact->AddMember("Model",               "\"FIVE_PARAMS\"");
      js_contact->AddMember("A",                   to_str(hammer.A));
      js_contact->AddMember("B",                   to_str(hammer.B));
      js_contact->AddMember("C",                   to_str(hammer.C));
      js_contact->AddMember("D",                   to_str(hammer.D));
      js_contact->AddMember("Damping",             to_str(hammer.damp));
    }
  }



  void
  Shank
  ::Init()
  {
    shank.InitEquation();
    hammer.InitEquation();

    Discretize(shank.length, Nx, order);
    AssembleFemMatrix(shank);

    InitBuffer();

    // We assume that origin point (0,0) is the center of the hammer pivot
    xi_x_minus = shank.length*cos(theta_init) - hammer.CoM_height*sin(theta_init);
    xi_x = xi_x_minus;
    xi_x_plus = xi_x_minus;
    xi_y_minus = shank.length*sin(theta_init) + hammer.CoM_height*cos(theta_init);
    xi_y = xi_y_minus;
    xi_y_plus = xi_y_minus;

    xi_x_dot_minus = 0.0;
    xi_x_dot = 0.0;
    xi_x_dot_plus = 0.0;
    xi_y_dot_minus = 0.0;
    xi_y_dot = 0.0;
    xi_y_dot_plus = 0.0;

    Q_minus.Reallocate(shape);      Q_minus.Zero();
    Q.Reallocate(shape);            Q.Zero();
    Q_plus.Reallocate(shape);       Q_plus.Zero();
    Q_tilde.Reallocate(shape);      Q_tilde.Zero();

    Q_dot_minus.Reallocate(shape);  Q_dot_minus.Zero();
    Q_dot.Reallocate(shape);        Q_dot.Zero();
    Q_dot_plus.Reallocate(shape);   Q_dot_plus.Zero();
    Q_dot_tilde.Reallocate(shape);  Q_dot_tilde.Zero();

    theta_plus = theta_init;
    theta = theta_init;
    theta_minus = theta_init;
    theta_tilde = 0.5*(3.0*theta - theta_minus);

    theta_dot_plus = 0.0;
    theta_dot = 0.0;
    theta_dot_minus = 0.0;
    theta_dot_tilde = 0.5*(3.0*theta_dot - theta_dot_minus);

    theta_ddot_plus = 0.0;
    theta_ddot = 0.0;
    theta_ddot_minus = 0.0;
    theta_ddot_tilde = 0.5*(3.0*theta_ddot - theta_ddot_minus);

    string_force = 0.0;
    coupling_force_x = 0.0;
    coupling_force_y = 0.0;

    Zh_minus = sqrt(auxiliary_constant_head);
    Zh_plus = sqrt(auxiliary_constant_head);

    Z_minus = ComputeAuxVarShank(Q_tilde, theta_tilde, theta_dot_tilde, auxiliary_constant_shank, shank);
    Z_plus = Z_minus;
  }



  void
  Shank
  ::ChooseTimeStepCFL(Real_wp &dt_piano)
  {
    // We use Arpack eigenvalue solver to compute
    // the spectral radius of Mh^{-1}Kh
    SparseEigenProblem<Real_wp, sym_srmatrix> arpack;

    arpack.SetDiagonalMass(true); // Mass lumping, Mh is diagonal

    sym_srmatrix Kh_sym; Copy(Kh, Kh_sym);
    arpack.InitMatrix(Kh_sym, Mh);
    arpack.SetNbAskedEigenvalues(1); // We need only one eigenvalue
    arpack.SetTypeSpectrum(arpack.LARGE_EIGENVALUES, Real_wp(0)); // The largest one
    arpack.SetComputationalMode(arpack.REGULAR_MODE);
    arpack.SetStoppingCriterion(1e-6);
    arpack.SetNbMaximumIterations(1e6);

    rvector lambda_real, lambda_imag;
    rmatrix eigen_vec;
    GetEigenvaluesEigenvectors(arpack, lambda_real, lambda_imag, eigen_vec);

    dt = sqrt(cfl_fraction / lambda_real(0)); // shank.dt
    dt_piano = dt;                            // piano.dt
  }



  void
  Shank
  ::ComputeSourceTerms()
  {
    normal_force.Reallocate(nb_iter+1);

    if (source_duration > 0.0)
    {
      const Real_wp t0 = source_start_time + 0.5*source_duration;
      const Real_wp sigma_t = 0.5*source_duration;
      Real_wp time = 0.0;
      for (unsigned iter = 0; iter < nb_iter+1; ++iter)
      {
        time = (iter + 0.5)*dt;
        normal_force(iter) = source_amplitude * Delta(time, t0, sigma_t);
      }
    }
  }





  void Shank::ComputeConstDiffMatrix(srmatrix &M) const
  {

    srmatrix temp(shape, shape);
    Add(Real_wp(2.0)/dt, Mh, temp);
    Add(Real_wp(0.5)*dt, Kh, temp);
    if (shank.damped)
    {
      Add(Real_wp(1.0), Rh, temp);
    }
    
    for (unsigned i = 0; i < shape; ++i)
    {
      for (unsigned j = 0; j < shape; ++j)
      {
        if (temp(i,j) != 0.0)
        {
          M.AddInteraction(offset+i, offset+j, temp(i,j));
        }
      }
    }


    for (unsigned i = 0; i < shape; ++i)
    {
      M.AddInteraction(offset+i,       offset+shape  ,  -2.0*MhVh(i)/dt);
      M.AddInteraction(offset+shape,   offset+i      ,  -2.0*MhVh(i)/dt);
      M.AddInteraction(offset+i,       offset+shape+1,  Ihw(i));
      M.AddInteraction(offset+shape+1, offset+i      ,  Ihw(i));
    }

    M.AddInteraction(offset+shape  , offset+shape  , 2.0*teta_coef_h/dt);
    M.AddInteraction(offset+shape+2, offset+shape+2, 2.0/dt);
    M.AddInteraction(offset+shape+3, offset+shape+3, 1.0);
    M.AddInteraction(offset+shape+4, offset+shape+4, 1.0);
    M.AddInteraction(offset+shape+5, offset+shape+5, 1.0);
    M.AddInteraction(offset+shape+6, offset+shape+6, 1.0);
    M.AddInteraction(offset+shape+7, offset+shape+7, 1.0);
    M.AddInteraction(offset+shape+8, offset+shape+8, 2.0/dt);

    M.AddInteraction(offset+shape+3, offset+shape+5, 2.0*hammer.mass/dt);
    M.AddInteraction(offset+shape+4, offset+shape+6, 2.0*hammer.mass/dt);
    M.AddInteraction(offset+shape+4, offset+shape+7, 1.0);
  }


  void Shank::ComputeDiffMatrix(srmatrix &M) const
  {
    for (unsigned i = 0; i < shape+1; ++i)
    {
      M.AddInteraction(offset+i,       offset+shape+2,  SAVvect(i));
      M.AddInteraction(offset+shape+2, offset+i      , -SAVvect(i));
    }

    const unsigned last_dof_w = shape-2;
    M.AddInteraction(offset+shape+5,    offset+last_dof_w, -sin(theta_tilde));
    M.AddInteraction(offset+shape+6,    offset+last_dof_w,  cos(theta_tilde));
    M.AddInteraction(offset+last_dof_w, offset+shape+3   , -sin(theta_tilde));
    M.AddInteraction(offset+last_dof_w, offset+shape+4   ,  cos(theta_tilde));

    M.AddInteraction(offset+shape+5, offset+shape, shank.length*sin(theta_tilde)
                  - (Q_tilde(last_dof_w) - hammer.CoM_height)*cos(theta_tilde));
    M.AddInteraction(offset+shape+6, offset+shape, -shank.length*cos(theta_tilde)
                  - (Q_tilde(last_dof_w) - hammer.CoM_height)*sin(theta_tilde));
    M.AddInteraction(offset+shape, offset+shape+3, shank.length*sin(theta_tilde)
                  - (Q_tilde(last_dof_w) - hammer.CoM_height)*cos(theta_tilde));
    M.AddInteraction(offset+shape, offset+shape+4, -shank.length*cos(theta_tilde)
                  - (Q_tilde(last_dof_w) - hammer.CoM_height)*sin(theta_tilde));
  }



  void Shank::ComputeRightHandSide(rvector &RHS) const
  {
    rvector temp(shape); temp.Zero();

    Add(Real_wp(-normal_force(current_iter)), rubber_repartition_w, temp);

    MltAdd(Real_wp(2.0)/dt, Mh, Q_dot, Real_wp(1.0), temp);
    MltAdd(Real_wp(-1.0), Kh, Q, Real_wp(1.0), temp);
    Add(Real_wp(-2.0)*theta_dot/dt, MhVh, temp);

    for (unsigned i = 0; i < shape; ++i)
    {
      RHS(offset+i) = temp(i);
    }

    RHS(offset+shape  ) = normal_force(current_iter) * rubber_repartition_theta
                        + 2.0*teta_coef_h*theta_dot/dt - 2.0*DotProd(MhVh, Q_dot)/dt;
    RHS(offset+shape+1) = 0;
    RHS(offset+shape+2) = 2.0*Z_minus/dt;
    RHS(offset+shape+3) = 2.0*hammer.mass*xi_x_dot/dt;
    RHS(offset+shape+4) = 2.0*hammer.mass*xi_y_dot/dt - hammer.mass*gravity;
    RHS(offset+shape+5) = 0;
    RHS(offset+shape+6) = 0;
    RHS(offset+shape+7) = 0;
    RHS(offset+shape+8) = 2.0*Zh_minus/dt;


    for (unsigned k = 0; k < shank.dimension; ++k)
    {
      if (shank.dirichlet_0(k))
      {
        RHS(offset+k) = 0.0;
      }
    }
  }



  void
  Shank
  ::OneStep(srmatrix &M, rvector &RHS)
  {
    SAVvect = AssembleSAV(Q_tilde, Q_dot_tilde,
                            theta_tilde, theta_dot_tilde, theta_ddot_tilde, 
                            auxiliary_constant_shank, shank);

    ComputeRightHandSide(RHS);
    ComputeDiffMatrix(M);
  }



  void
  Shank
  ::ExtractNewSol(const rvector &SOL)
  {
    rvector muP(shape);
    for (unsigned i = 0; i < shape; ++i)
    {
      muP(i) = SOL(offset+i);
    }

    Q_plus = Q + dt*muP;
    Q_dot_plus = Real_wp(2.0)*muP - Q_dot;
    //DISP(DotProd(Ihw, Q_plus)); // Should always be ~0

    theta_plus = theta + dt*SOL(offset+shape);
    theta_dot_plus = 2.0*SOL(offset+shape) - theta_dot;
    theta_ddot_plus = 2.0*(theta_dot_plus - theta_dot)/dt - theta_ddot;

    Z_plus = 2.0*SOL(offset+shape+2) - Z_minus;

    xi_x_plus = xi_x + dt*SOL(offset+shape+5);
    xi_y_plus = xi_y + dt*SOL(offset+shape+6);
    xi_x_dot_plus = 2.0*SOL(offset+shape+5) - xi_x_dot;
    xi_y_dot_plus = 2.0*SOL(offset+shape+6) - xi_y_dot;

    Zh_plus = 2.0*SOL(offset+shape+8) - Zh_minus;

    string_force = SOL(offset+shape+7);
  }



  void
  Shank
  ::UpdateValues()
  {
    xi_x_minus = xi_x;
    xi_x = xi_x_plus;

    xi_y_minus = xi_y;
    xi_y = xi_y_plus;

    Q_minus = Q;
    Q = Q_plus;
    Q_tilde = Real_wp(0.5)*(Real_wp(3.0)*Q - Q_minus);

    Q_dot_minus = Q_dot;
    Q_dot = Q_dot_plus;
    Q_dot_tilde = Real_wp(0.5)*(Real_wp(3.0)*Q_dot - Q_dot_minus);

    theta_minus = theta;
    theta = theta_plus;
    theta_tilde = 0.5*(3.0*theta - theta_minus);

    theta_dot_minus = theta_dot;
    theta_dot = theta_dot_plus;
    theta_dot_tilde = 0.5*(3.0*theta_dot - theta_dot_minus);

    theta_ddot_minus = theta_ddot;
    theta_ddot = theta_ddot_plus;
    theta_ddot_tilde = 0.5*(3.0*theta_ddot - theta_ddot_minus);

    xi_x_dot_minus = xi_x_dot;
    xi_x_dot = xi_x_dot_plus;

    xi_y_dot_minus = xi_y_dot;
    xi_y_dot = xi_y_dot_plus;

    Zh_minus = Zh_plus;
    Z_minus = Z_plus;

  }



  void Shank::ApplyBoundaryConditions(srmatrix &M) const
  {
    Real_wp mem;
    // Boundary conditions at x=0
    for (unsigned k = 0; k < shank.dimension; ++k)
    {
      if (shank.dirichlet_0(k))
      {
        mem = M.Get(offset+k,offset+k);
        M.ClearRow(offset+k);
        M.AddInteraction(offset+k, offset+k, mem);
      }
    }
  }



  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  //    ENERGY
  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-


  Real_wp Shank::ComputeKineticEnergy() const
  {
    rvector res(shape), temp(shape);
    temp = Q_dot_plus - theta_dot_plus*Vh;
    Mlt(Mh, temp, res);
    return 0.5*DotProd(res, temp)
                + 0.5*hammer.mass*square(xi_x_dot_plus)
                + 0.5*hammer.mass*square(xi_y_dot_plus);
  }


  Real_wp Shank::ComputePotentialEnergy() const
  {
    rvector res(shape);
    Mlt(Kh, Q_plus, res);
    return 0.5*DotProd(res, Q_plus) + hammer.mass*gravity*xi_y_plus;
  }


  Real_wp Shank::ComputeNonLinearEnergy() const
  {
    return 0.5*Z_plus*Z_plus + 0.5*Zh_plus*Zh_plus;
  }


  Real_wp Shank::ComputeDissipatedEnergy() const
  {
    // Rajouter une dissipation sur l'angle theta
    Real_wp dissip_energy = 0.0;

    rvector res(shape), temp(shape);
    temp = Real_wp(0.5)*(Q_dot_plus + Q_dot);

    dissip_energy += -normal_force(current_iter) * DotProd(temp, rubber_repartition_w);
    dissip_energy += 0.5*(theta_dot_plus + theta_dot)
                          *normal_force(current_iter)*rubber_repartition_theta;

    if (shank.damped)
    {
      Mlt(Rh, temp, res);
      dissip_energy -= DotProd(res, temp);
    }

    return dissip_energy;
  }



  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  //    OUTPUTS
  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-



  void
  Shank
  ::InitBuffer()
  {
    if (output_on_ddls)
    {
      outputs_shank.Reallocate(shape);
    }
    else if (output_on_nodes)
    {
      outputs_shank.Reallocate(Nx);
    }
    else
    {
      outputs_shank.Reallocate(Nx_output);
    }
    for (unsigned p = 0; p < outputs_shank.GetM(); ++p)
    {
      outputs_shank(p).SetDoublePrecision();
      outputs_shank(p).Init(file_base + file_output_shank
                              + "_Point" + to_str(p) + ".txt",
                              size_buffer);
    }

    if (interp_grad)
    {
      if (output_on_ddls)
      {
        outputs_shank_grad.Reallocate(shape);
      }
      else if (output_on_nodes)
      {
        outputs_shank_grad.Reallocate(Nx);
      }
      else
      {
        outputs_shank_grad.Reallocate(Nx_output);
      }
      for (unsigned p = 0; p < outputs_shank_grad.GetM(); ++p)
      {
        outputs_shank_grad(p).SetDoublePrecision();
        outputs_shank_grad(p).Init(file_base + file_output_shank_grad
                                      + "_Point" + to_str(p) + ".txt",
                                      size_buffer);
      }
    }

    output_theta.SetDoublePrecision();
    output_theta.Init(file_base+file_output_theta+".txt", size_buffer);

    output_head.SetDoublePrecision();
    output_head.Init(file_base+file_output_head+".txt", size_buffer);

    if (output_aux_var)
    {
      output_auxiliary.SetDoublePrecision();
      output_auxiliary.Init(file_base+file_output_aux_var+".txt", size_buffer);
    }

    interp_values.Reallocate(shank.dimension);
    interp_values_plus.Reallocate(shank.dimension);
    if (interp_grad)
    {
      interp_values_grad.Reallocate(shank.dimension);
      interp_values_grad_plus.Reallocate(shank.dimension);
    }
  }





  Vector<rvector> Shank::ExtractCompsFromQ(const rvector &X) const
  {
    Vector<rvector> res(shank.dimension);
    for (unsigned k = 0; k < shank.dimension; ++k)
    {
      res(k).Reallocate(nH1);
      for (unsigned p = 0; p < nH1; ++p)
      {
        res(k)(p) = X(k + shank.dimension*p);
      }
    }
    return res;
  }




  void Shank::WriteSismos(const DiscretizedBeam &grid_interp_time)
  {
    bool already_computed = false; // do not recompute the interp etc if there are 2 output points during one single iteration
    for (unsigned l = 0; l < grid_interp_time.Nx_output; ++l)
    {
      const unsigned ne_loc = grid_interp_time.output_loc_on_mesh(l);
      if (ne_loc == current_iter)
      {
        const Real_wp t = grid_interp_time.output_coord_on_mesh(l);
        const Real_wp current_time_output = dt_outputs*l;

        if (!already_computed)
        {
          if (output_on_ddls)
          {
            interp_values = ExtractCompsFromQ(Q);
            interp_values_plus = ExtractCompsFromQ(Q_plus);
            if (interp_grad)
            {
              std::cout << "\033(31mERROR: no gradient output "
                   << "implemented for Ddls.\033(0m" << std::endl;
              abort();
            }
          }
          else if (output_on_nodes)
          {
            const Vector<rvector> WPhi = ExtractCompsFromQ(Q);
            const Vector<rvector> WPhi_plus = ExtractCompsFromQ(Q_plus);
            for (unsigned k = 0; k < shank.dimension; ++k)
            {
              interp_values(k).Reallocate(Nx);
              interp_values_plus(k).Reallocate(Nx);
              for (unsigned p = 0; p < Nx; ++p)
              {
                interp_values(k)(p) = WPhi(k)(order*p);
                interp_values_plus(k)(p) = WPhi_plus(k)(order*p);
              }
            }
            if (interp_grad)
            {
              std::cout << "\033(31mERROR: no gradient output "
                   << "implemented for nodes.\033(0m" << std::endl;
              abort();
            }
          }
          else
          {
            const Vector<rvector> WPhi = ExtractCompsFromQ(Q);
            const Vector<rvector> WPhi_plus = ExtractCompsFromQ(Q_plus);
            for (unsigned k = 0; k < shank.dimension; ++k)
            {
              interp_values(k).Reallocate(Nx_output);
              interp_values_plus(k).Reallocate(Nx_output);
              Mlt(interp_H1, WPhi(k), interp_values(k));
              Mlt(interp_H1, WPhi_plus(k), interp_values_plus(k));
              if (interp_grad)
              {
                interp_values_grad(k).Reallocate(Nx_output);
                interp_values_grad_plus(k).Reallocate(Nx_output);
                Mlt(grad_interp_H1, WPhi(k), interp_values_grad(k));
                Mlt(grad_interp_H1, WPhi_plus(k), interp_values_grad_plus(k));
              }
            }
          }
          already_computed = true;
        }

        rvector temp(shank.dimension+1);
        temp(0) = current_time_output;

        for (unsigned p = 0; p < interp_values(0).GetM(); ++p)
        {
          for (unsigned k = 0; k < shank.dimension; ++k)
          {
            temp(k+1) = interp_values(k)(p)*(1.0-t)
                          + interp_values_plus(k)(p)*t;
          }
          outputs_shank(p).AddVect(temp);
        }

        if (interp_grad)
        {
          for (unsigned p = 0; p < interp_values_grad(0).GetM(); ++p)
          {
            for (unsigned k = 0; k < shank.dimension; ++k)
            {
              temp(k+1) = interp_values_grad(k)(p)*(1.0-t)
                            + interp_values_grad_plus(k)(p)*t;
            }
            outputs_shank_grad(p).AddVect(temp);
          }
        }

        rvector temp_theta(2);
        temp_theta(0) = current_time_output;
        temp_theta(1) = theta*(1.0-t) + theta_plus*t;
        output_theta.AddVect(temp_theta);

        rvector temp_head(3);
        temp_head(0) = current_time_output;
        temp_head(1) = xi_x*(1.0-t) + xi_x_plus*t;
        temp_head(2) = xi_y*(1.0-t) + xi_y_plus*t;
        output_head.AddVect(temp_head);

        if (output_aux_var)
        {
          rvector temp_aux(3);
          temp_aux(0) = current_time_output;
          temp_aux(1) = Z_minus*(1.0-t) + Z_plus*t;
          temp_aux(2) = Zh_minus*(1.0-t) + Zh_plus*t;
          output_auxiliary.AddVect(temp_aux);
        }
      }
    }
  }


} // namespace