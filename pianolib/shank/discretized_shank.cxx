/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{


  DiscretizedShank::DiscretizedShank() : DiscretizedBeam() {}


  void
  DiscretizedShank
  ::AssembleFemMatrix(const StiffShank &shank)
  {
    shape = shank.dimension * nH1;

    rubber_repartition_w = AssembleRepartition(0, shank);
    rubber_repartition_phi = AssembleRepartition(1, shank);
    rubber_repartition_theta = ComputeIntegraleRepartitionTheta(shank);

    Vh.Reallocate(shape);
    MhVh.Reallocate(shape);
    Ihw.Reallocate(shape);
    Mh.Reallocate(shape, shape);
    Kh.Reallocate(shape, shape);
    Rh.Reallocate(shape, shape);

    Add(Real_wp(1), AssembleMassMatrix(shank.M), Mh);
    Vh = AssemblePositionVector(shank);
    Mlt(Mh, Vh, MhVh);
    teta_coef_h = DotProd(MhVh, Vh);

    Ihw = AssembleIntegralVector(shank);

    if (shank.damped)
    {
      Add(Real_wp(1), AssembleMassMatrix(shank.R), Rh);
      Add(Real_wp(1), AssembleStiffnessMatrix(shank.H), Rh);
    }

    Add(Real_wp(1), AssembleStiffnessMatrix(shank.A), Kh);
    Add(Real_wp(1), AssembleMassMatrix(shank.C), Kh);
    srmatrix Bh = AssembleSemiGradMatrix(shank.B);
    Add(Real_wp(1), Bh, Kh);
    Transpose(Bh); // Bh <- transposed(Bh);
    Add(Real_wp(1), Bh, Kh);

    SAVvect.Reallocate(shape);
  }



  void
  DiscretizedShank
  ::OutputFemMatrices(const std::string &file) const
  {
    Mh.WriteText(file + "Mh_shank.txt");
    Kh.WriteText(file + "Kh_shank.txt");
    MhVh.WriteText(file + "MhVh_shank.txt");
    Ihw.WriteText(file + "Ihw_shank.txt");
    Rh.WriteText(file + "Rh_shank.txt");
  }



  rvector
  DiscretizedShank
  ::AssembleRepartition(const unsigned &component, const StiffShank &shank) const
  {
    rvector res(shape); res.Zero();
    Real_wp temp, x;
    unsigned ii;

    if (shank.rubber_width > 0)
    {
      for (unsigned ne = 0; ne < Nx-1; ++ne)
      {
        for (unsigned i = 0; i < nb_base; ++i)
        {
          ii = dof_num_H1(ne,i);
          x = (ne + quad_points(i))*dx;
          temp = quad_weights(i)*Repartition(x, shank.rubber_slope, 
                                                shank.rubber_position, 
                                                shank.rubber_width);

          if(abs(temp) > 1e-12) // Not very good
          {
            res(shank.dimension*ii + component) += dx*temp;
          }
        }
      }
    }
    return res;
  }



  Real_wp
  DiscretizedShank
  ::ComputeIntegraleRepartitionTheta(const StiffShank &shank) const
  {
    Real_wp x, res = 0.0;
    if (shank.rubber_width > 0)
    {
      for (unsigned ne = 0; ne < Nx-1; ++ne)
      {
        for (unsigned g = 0; g < nb_base; ++g)
        {
          x = (ne + quad_points(g))*dx;
          res += quad_weights(g)*x*Repartition(x, shank.rubber_slope, 
                                                  shank.rubber_position, 
                                                  shank.rubber_width);
        }
      }
    }
    return dx*res;
  }



  //! Assemble mass vector \int M \cdot \phi_i
  rvector
  DiscretizedShank
  ::AssemblePositionVector(const StiffShank &shank, const bool &use_H1) const
  {
    const size_t size = shank.dimension;
    rvector int_vector;

    if (use_H1)
    {
      int_vector.Reallocate(nH1*size); int_vector.Fill(1);
    }
    else
    {
      int_vector.Reallocate(nL2*size); int_vector.Fill(1);
    }

    unsigned ii;
    for (unsigned ne = 0; ne < Nx-1; ++ne) // Loop on elements
    {
      for (unsigned i = 0; i < nb_base; ++i) // Loop on test functions
      {
        if (use_H1)
        {
          ii = dof_num_H1(ne,i);
        }
        else
        {
          ii = dof_num_L2(ne,i);
        }
        int_vector(size*ii) = (ne + quad_points(i))*dx; // += ??
      }
    }
    return int_vector;
  }



  //! Assemble integral vector \int \phi_i
  rvector
  DiscretizedShank
  ::AssembleIntegralVector(const StiffShank &shank, const bool &use_H1) const
  {
    const size_t size = shank.dimension;
    rvector int_vector;
  
    if (use_H1)
    {
      int_vector.Reallocate(nH1*size); int_vector.Zero();
    }
    else
    {
      int_vector.Reallocate(nL2*size); int_vector.Zero();
    }

    unsigned ii;
    for (unsigned ne = 0; ne < Nx-1; ++ne) // Loop on elements
    {
      
      for (unsigned i = 0; i < nb_base; ++i) // Loop on test functions
      {
        if (use_H1)
        {
          ii = dof_num_H1(ne,i);
        }
        else
        {
          ii = dof_num_L2(ne,i);
        }
        int_vector(size*ii) += dx*quad_weights(i);
      }
    }
    return int_vector;
  }




  rvector
  DiscretizedShank
  ::AssembleSAV(const rvector &Q_tilde, const rvector &dQ_tilde,
                const Real_wp &T_tilde, const Real_wp &dT_tilde, const Real_wp &ddT_tilde,
                const Real_wp &auxiliary_constant, const StiffShank &shank) const
  {
    rvector SAV(shape+1); SAV.Zero();
    rvector SAV_G(shape+1); SAV_G.Zero();
    rvector SAV_L(shape+1); SAV_L.Zero();
    SAV_G = AssembleSAVGrad(T_tilde, shank);
    SAV_L = AssembleSAVLagragian(Q_tilde, dQ_tilde, T_tilde, dT_tilde, ddT_tilde, shank);
    SAV = (SAV_G + SAV_L) / ComputeAuxVarShank(Q_tilde, T_tilde, dT_tilde, auxiliary_constant, shank);
    return SAV;
  }




  rvector
  DiscretizedShank
  ::AssembleSAVGrad(const Real_wp &T_tilde, const StiffShank &shank) const
  {
    rvector res(shape);
    res = -Ihw;
    res.PushBack( 0.5*square(shank.length) );

    const Real_wp coef = shank.density * shank.section * gravity * cos(T_tilde);
    Mlt(coef, res);
    return res;
  }



  rvector
  DiscretizedShank
  ::AssembleSAVLagragian(const rvector &Q_tilde, const rvector &dQ_tilde,
                          const Real_wp &T_tilde, const Real_wp &dT_tilde,
                          const Real_wp &ddT_tilde, const StiffShank &shank) const
  {
    const unsigned dim = shank.dimension;

    unsigned ii;
    rvector res(shape+1); res.Zero();
    rvector qh(dim), qh_dot(dim), sumquad_q(dim), res_q(dim);
    Real_wp sumquad_t, res_t = 0.0;

    for (unsigned ne = 0; ne < Nx-1; ++ne) // Loop on elements
    {
      sumquad_t = 0.0;
      for (unsigned i = 0; i < nb_base; ++i) // Loop on test functions
      {
        ii = dof_num_H1(ne,i);
        for (unsigned k = 0; k < dim; ++k)
        {
          qh(k) = Q_tilde(ii*dim + k);
          qh_dot(k) = dQ_tilde(ii*dim + k);
        }

        shank.ComputeEL_q(qh, dT_tilde, res_q);
        shank.ComputeEL_theta(qh, qh_dot, dT_tilde, ddT_tilde, res_t);
        sumquad_q = res_q * quad_weights(i);
        sumquad_t += res_t * quad_weights(i);

        for (unsigned n = 0; n < dim; ++n)
        {
          res(ii*dim + n) += sumquad_q(n);
        }
      }
      res(shape) += sumquad_t;
    }
    Mlt(dx, res);
    return res;
  }



  Real_wp
  DiscretizedShank
  ::ComputeAuxVarShank(const rvector &Q_tilde,
                        const Real_wp &T_tilde, const Real_wp &dT_tilde,
                        const Real_wp &auxiliary_constant, const StiffShank &shank) const
  {
    return sqrt(2.0*shank.ComputeIntegraleU(T_tilde)
                  + 2.0*ComputeIntegraleLagrangian(shank, Q_tilde, dT_tilde)
                                        + auxiliary_constant);
  }




  Real_wp
  DiscretizedShank
  ::ComputeIntegraleLagrangian(const StiffShank &shank,
                                const rvector &V,
                                const Real_wp &dT_tilde) const
  {
    const unsigned dim = shank.dimension;

    unsigned gg;
    Real_wp sumquad, integrale = 0.0;
    rvector vh(dim);

    for (unsigned ne = 0; ne < Nx-1; ++ne) // Loop on elements
    {
      sumquad = 0.0;
      for (unsigned g = 0; g < nb_base; ++g) // Loop on integration points
      {
        gg = dof_num_H1(ne,g);
        for (unsigned k = 0; k < dim; ++k)
        {
          vh(k) = V(gg*dim + k);
        }

        sumquad += shank.ComputeLagrangian(vh, dT_tilde)*quad_weights(g);
      }
      integrale += sumquad*dx;
    }
    return integrale;
  }



} // namespace