/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{

  //! Class for the coupling of all elements
  class Piano : public DataFileReader
  {

  public:

    Shank shank;
    PianoString pstring;


    SparseDirectSolver<Real_wp> solver;

    Real_wp dt;
    unsigned nb_iter, current_iter;
    Real_wp duration, current_time;

    Real_wp cfl_fraction;

    unsigned shape;
    srmatrix DiffMatrix;
    srmatrix ConstDiffMatrix;
    rvector RHS;

    rvector repartition_hammer;
    bool remove_hammer, hammer_is_removed;
    Real_wp remove_hammer_threshold;
    bool contact_start, contact_stop;
    Real_wp start_time_contact, contact_duration;
    Real_wp speed_contact_trans, speed_contact_longi;
    

    rvector LM_sndb, LM_sndb_plus; // Lagrange multipliers for soundboard coupling


    // File names
    std::string file_base;
    const std::string file_output_energy = "Energy";

    WriteOnTheGoWithBuffer output_energy;

    unsigned size_buffer;
    bool compute_energy;
    bool interp_grad;
    bool output_fem_matrices;
    bool output_on_ddls, output_on_nodes;
    bool output_aux_var;

    bool nan_detected;

    Real_wp dt_outputs;
    DiscretizedBeam grid_interp_time; // Grid for time interp

    WriteOnTheGoWithBuffer output_LM_sndb;
    const std::string file_output_LM_sndb = "Sismo_LM";

    const std::string file_summary = "summary.json";

    Timer timer_ALL, timer_INIT, timer_ENERGY, timer_OUTPUT;
    Timer timer_FACTO, timer_SOLVE, timer_FILL, timer_BOUND;
    Timer timer_DEBUG;


    Piano();

    void SetInputData(const std::string &keyword, const Vector<std::string> &param);

    void DisplayInit(std::ostream &out) const;
    void WriteSummaryBegin(JsonManager* jsm) const;
    void DisplaySimuAdvance();
    void DisplayFinalize(std::ostream &out) const;
    void WriteSummaryEnd(JsonManager* jsm) const;

    void Init();

    void ChooseTimeStepCFL();

    void ApplyBoundaryConditions(srmatrix &M) const;

    void OneStep();

    void RunSimulation(JsonManager* jsm);

    void UpdateValues();

    void OutputFemMatrices();

    void ConstructTimeInterp();

    void ComputeSourceTerms();

    void InitBuffer();

    void WriteSismos();

    void WriteEnergy();



    //*****************************//
    //  In file hammer_string.cxx  //
    //*****************************//

    void ComputeRepartitionHammerString();

    void ComputeConstDiffMatrixHammerStringCoupling(srmatrix &M) const;
    void ComputeDiffMatrixHammerStringCoupling(srmatrix &M) const;

    void CheckHammerStuff();

    Real_wp ComputeDissipatedEnergyHammerStringCoupling() const;



    //*****************************//
    //  In file hammer_string.cxx  //
    //*****************************//

    void ComputeConstDiffMatrixBridgeCoupling(srmatrix &M) const;
    void ComputeDiffMatrixBridgeCoupling(srmatrix &M) const;


  };
  
} // namespace