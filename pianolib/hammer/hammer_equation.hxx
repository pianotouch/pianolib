/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{


  class HammerHead
  {
  public:

    // Physical parmeters
    Real_wp d0;
    Real_wp top_height; // Distance between the center of mass and the top of the hammer
    Real_wp CoM_height; // Distance between the center of mass and the shank
    Real_wp mass;
    Real_wp width;
    Real_wp slope;

    Real_wp string_height; // Distance between the pivot of the shank and the string
    Real_wp impact_point; // Position of the stroke of the hammer on the string

    const Real_wp contact_threshold = 1e-9;

    int contact_model; enum{THREE_PARAMS, FIVE_PARAMS};

    Real_wp p, K, R; // Coefficients for THREE_PARAMS contact model
    Real_wp A, B, C, D, damp; // Coefficients for FIVE_PARAMS contact model


    //! default constructor
    HammerHead()
    {
      top_height = 0.027659;
      CoM_height = 0.025;
      mass = 12.09e-3;
      width = 0.02;
      slope = 2000;

      impact_point = 1.0/8.0;
      string_height = 0.0565;
      d0 = top_height - string_height;

      contact_model = FIVE_PARAMS;
      A = -5.0617e12;
      B = 1.879e10;
      C = 9.1872e6;
      D = 4459.6;
      damp = 0.8;
    }


    inline void
    InitEquation()
    {
      d0 = top_height - string_height;
    }


    inline Real_wp
    Phi1(const Real_wp &x) const
    {
      if (contact_model == THREE_PARAMS)
      {
        if ((d0 - x) > contact_threshold)
        {
          return K * pow(d0 - x, p);
        }
      }
      else if (contact_model == FIVE_PARAMS)
      {
        if ((d0 - x) > contact_threshold)
        {
          return A * pow(d0 - x, 4)
                + B * pow(d0 - x, 3)
                + C * pow(d0 - x, 2)
                + D * (d0 - x);
        }
      }
      return 0.0;
    }


    inline Real_wp
    Phi2(const Real_wp &x) const
    {
      if (contact_model == THREE_PARAMS)
      {
        if ((d0 - x) > contact_threshold)
        {
          return R * p * pow(d0 - x, p-1.0);
        }
      }
      else if (contact_model == FIVE_PARAMS)
      {
        if ((d0 - x) > contact_threshold)
        {
          return damp * Phi1(x);
        }
      }
      return 0.0;
    }


    // Primitive of function Phi1
    inline Real_wp
    Psi(const Real_wp &x) const
    {
      if (contact_model == THREE_PARAMS)
      {
        if ((d0 - x) > contact_threshold)
        {
          return K / (p + 1.0) * pow(d0 - x, p+1.0);
        }
      }
      else if (contact_model == FIVE_PARAMS)
      {
        if ((d0 - x) > contact_threshold)
        {
          return A * pow(d0 - x, 5) /5.0
               + B * pow(d0 - x, 4) /4.0
               + C * pow(d0 - x, 3) /3.0
               + D * pow(d0 - x, 2) /2.0;
        }
      }
      return 0.0;
    }



    // Auxiliary function
    inline Real_wp
    G(const Real_wp &x, const Real_wp &auxiliary_constant) const
    {
      if (auxiliary_constant == 0.0)
      {
        if (contact_model == THREE_PARAMS)
        {
          if (p-1.0 < 0.0)
          {
            std::cout << "ERROR: Your model is not well-posed." << std::endl;
            abort();
          }
          if((d0 - x) > contact_threshold)
          {
            return -sqrt(0.5*K*(p+1.0)) * pow(d0 - x, 0.5*(p-1.0));
          }
        }
        else if (contact_model == FIVE_PARAMS)
        {
          std::cout << "ERROR: Please specify a non-zero auxiliary constant." << std::endl;
          abort();
        }
      }
      else
      {
        return -Phi1(x) / sqrt(2.0*Psi(x) + auxiliary_constant);
      }
      return 0.0;
    }


  };


} // namespace