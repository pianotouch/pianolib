/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{

  class GenericBeamEquation;

  class DiscretizedString : public DiscretizedBeam
  {

  public:

    sym_srmatrix Mh;
    srmatrix Rh, Kh;

    rvector SAVvect;

    unsigned shape;


    DiscretizedString();

    void AssembleFemMatrix(const GenericBeamEquation &model);

    void OutputFemMatrices(const std::string &file) const;

    rvector
    AssembleSAV(const rvector &Q_tilde, const Real_wp &auxiliart_constant, 
                                        const GenericBeamEquation &model) const;

    Real_wp ComputeAuxVarSAV(const rvector &Q_tilde, const Real_wp &auxiliart_constant,
                                                  const GenericBeamEquation &model) const;

    Real_wp ComputeIntegraleUtilde(const GenericBeamEquation &model, const rvector &V) const;

  };

} // namespace