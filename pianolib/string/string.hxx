/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{

  class PianoString : public DiscretizedString, public DataFileReader
  {
  public:

    GenericBeamEquation *model;

    Real_wp dt;
    unsigned nb_iter, current_iter;
    Real_wp duration, current_time;

    Real_wp cfl_fraction;
    Real_wp f_max_converged;

    unsigned offset; // Position of the string variables in the global piano DiffMatrix

    // String displacement
    rvector Q_plus, Q, Q_minus, Q_tilde;
    rvector Q_dot_plus, Q_dot, Q_dot_minus;

    // Auxiliary variable
    Real_wp Z_plus, Z_minus;

    // Auxiliary constant
    Real_wp auxiliary_constant;

    // Values of theta for Theta schemes
    Real_wp theta_slow, theta_fast;


    // Outputs for the string: the values of the variables
    // and their gradients when asked.
    Vector<WriteOnTheGoWithBuffer> outputs_string, outputs_string_grad;

    Vector<WriteOnTheGoWithBuffer> output_auxiliary;

    Vector<rvector> interp_values, interp_values_plus;
    Vector<rvector> interp_values_grad, interp_values_grad_plus;

    // File names
    std::string file_base;
    const std::string file_output_string = "Sismo_String";
    const std::string file_output_string_grad = "Sismo_String_Grad";
    const std::string file_output_aux_var = "Sismo_String_AuxVar";

    unsigned size_buffer;
    bool interp_grad;
    bool compute_energy;
    bool output_fem_matrices;
    bool output_on_ddls, output_on_nodes;
    bool output_aux_var;

    Real_wp dt_outputs;


  public:

    PianoString();

    void SetInputData(const std::string &keyword, const Vector<std::string> &param);

    void DisplayInit(std::ostream &out) const;
    void WriteJsonSummary(JsonManager* jsm) const;

    void Init();

    void ChooseTimeStepCFL(Real_wp &dt_piano);

    void ComputeConstDiffMatrix(srmatrix &M) const;

    void ComputeDiffMatrix(srmatrix &M) const;

    void OneStep(srmatrix &M, rvector &RHS);

    void ExtractNewSol(const rvector &SOL);

    void UpdateValues();

    void ApplyBoundaryConditions(srmatrix &M) const;

    void ComputeRightHandSide(rvector &RHS) const;

    Real_wp ComputeKineticEnergy() const;
    Real_wp ComputePotentialEnergy() const;
    Real_wp ComputeNonLinearEnergy() const;
    Real_wp ComputeDissipatedEnergy() const;

    void InitBuffer();

    void ConstructTimeInterp();

    Vector<rvector> ExtractCompsFromQ(const rvector &X) const;

    void WriteSismos(const DiscretizedBeam &grid_interp_time);


  };

} // namespace