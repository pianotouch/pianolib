/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{


  DiscretizedString::DiscretizedString() : DiscretizedBeam(){}



  void
  DiscretizedString
  ::AssembleFemMatrix(const GenericBeamEquation &model)
  {
    shape = model.dimension * nH1;

    Mh.Reallocate(shape, shape);
    Kh.Reallocate(shape, shape);
    Rh.Reallocate(shape, shape);

    Add(Real_wp(1), AssembleMassMatrix(model.M), Mh);
    Add(Real_wp(1), AssembleStiffnessMatrix(model.Aslow), Kh);
    Add(Real_wp(1), AssembleStiffnessMatrix(model.Afast), Kh);

    if (model.damped)
    {
      Add(Real_wp(1), AssembleMassMatrix(model.R), Rh);
      Add(Real_wp(1), AssembleStiffnessMatrix(model.H), Rh);
    }

    if (model.stiff)
    {
      Add(Real_wp(1), AssembleMassMatrix(model.C), Kh);
      srmatrix Bh = AssembleSemiGradMatrix(model.B);
      Add(Real_wp(1), Bh, Kh);
      Transpose(Bh); // Bh <- transposed(Bh);
      Add(Real_wp(1), Bh, Kh);
    }

    SAVvect.Reallocate(shape);
  }



  void
  DiscretizedString
  ::OutputFemMatrices(const std::string &file) const
  {
    Mh.WriteText(file + "Mh_string.txt");
    Kh.WriteText(file + "Kh_string.txt");
    Rh.WriteText(file + "Rh_string.txt");
  }



  rvector
  DiscretizedString
  ::AssembleSAV(const rvector &Q_tilde, const Real_wp &auxiliart_constant, 
                                        const GenericBeamEquation &model) const
  {
    const unsigned dim = model.dimension;

    unsigned ii, ll;
    rvector res(shape); res.Zero();
    rvector dx_qh(dim), sumquad(dim), res1(dim);
    const Real_wp aux_var = ComputeAuxVarSAV(Q_tilde, auxiliart_constant, model);
    
    for (unsigned ne = 0; ne < Nx-1; ++ne) // Loop on elements
    {
      for (unsigned i = 0; i < nb_base; ++i) // Loop on test functions
      {
        ii = dof_num_H1(ne,i);
        sumquad.Zero();
        for (unsigned j = 0; j < nb_base; ++j) // Loop on integration points
        {
          dx_qh.Zero();
          for (unsigned l = 0; l < nb_base; ++l) // Loop on basis functions
          {
            ll = dof_num_H1(ne,l);
            for (unsigned k = 0; k < dim; ++k)
            {
              dx_qh(k) += Q_tilde(ll*dim + k) * valdPhi(l,j);
            }
          }
          dx_qh = dx_qh / dx;
          model.ComputeGradUtilde(dx_qh, res1);
          sumquad += res1*quad_weights(j)*valdPhi(i,j);
        }
        for (unsigned n = 0; n < dim; ++n)
        {
          res(dim*ii + n) += sumquad(n) / aux_var;
        }
      }
    }
    return res;
  }



  Real_wp
  DiscretizedString
  ::ComputeAuxVarSAV(const rvector &Q_tilde, const Real_wp &auxiliary_constant, 
                                            const GenericBeamEquation &model) const
  {
    return sqrt(2.0 * ComputeIntegraleUtilde(model, Q_tilde) + auxiliary_constant);
  }



  Real_wp
  DiscretizedString
  ::ComputeIntegraleUtilde(const GenericBeamEquation &model,
                              const rvector &V) const
  {
    const unsigned dim = model.dimension;

    unsigned jj;
    Real_wp sumquad, integrale = 0.0;
    rvector dx_vh(dim);

    
    for (unsigned ne = 0; ne < Nx-1; ++ne) // Loop on elements
    {
      sumquad = 0.0;
      for (unsigned g = 0; g < nb_base; ++g) // Loop on integration points
      {
        dx_vh.Zero();
        for (unsigned j = 0; j < nb_base; ++j) // Loop on basis functions
        {
          jj = dof_num_H1(ne,j);
          for (unsigned k = 0; k < dim; ++k)
          {
            dx_vh(k) += V(jj*dim + k)*valdPhi(j,g);
          }
        }
        dx_vh = dx_vh / dx;
        Real_wp res1 = 0.0;
        model.ComputeUtilde(dx_vh, res1);
        sumquad += res1*quad_weights(g);
      }
      integrale += sumquad*dx;
    }
    return integrale;
  }



} // namespace