/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{


  PianoString::PianoString() : DiscretizedString()
  {
    model = new GenericBeamEquation();

    duration = 0.02;
    dt = -1;
    cfl_fraction = 10;

    current_time = 0.0;
    current_iter = 0;

    auxiliary_constant = 1.0;
    theta_slow = 1.0/4.0;
    theta_fast = 1.0/4.0;

    file_base = "./";
    interp_grad = false;
    compute_energy = false;
    output_on_ddls = false;
    output_on_nodes = false;
    output_fem_matrices = false;
    output_aux_var = false;
    dt_outputs = 1e-5;
    size_buffer = 100;

  }



  void
  PianoString
  ::SetInputData(const std::string &keyword, const Vector<std::string> &param)
  {
    if (keyword == "StringModel")
    {
      if (param(0) == "LIN")
        model = new LinearBeam(*model);
      else if (param(0) == "StiffLIN")
        model = new StiffLinearBeam(*model);
      else if (param(0) == "NL")
        model = new NonLinearBeam(*model);
      else if (param(0) == "StiffNL")
        model = new StiffNonLinearBeam(*model);
    }

    else if (keyword == "String") // Physical params of one 
    {
      model->length      = to_num<Real_wp>(param(0));
      model->section     = to_num<Real_wp>(param(1));
      model->density     = to_num<Real_wp>(param(2));
      model->tension     = to_num<Real_wp>(param(3));
      model->young_mod   = to_num<Real_wp>(param(4));
      model->quad_mod    = to_num<Real_wp>(param(5));
      model->shear_mod   = to_num<Real_wp>(param(6));
      model->kappa       = to_num<Real_wp>(param(7));

      model->dampR(0)    = to_num<Real_wp>(param(8));
      model->dampR(1)    = to_num<Real_wp>(param(9));
      model->dampR(2)    = to_num<Real_wp>(param(10));

      model->dampH(0)    = to_num<Real_wp>(param(11));
      model->dampH(1)    = to_num<Real_wp>(param(12));
      model->dampH(2)    = to_num<Real_wp>(param(13));
    }

    else if (keyword == "FemOrderString")
    {
      order = to_num<Real_wp>(param(0));
    }
    else if (keyword == "NxString")
    {
      if (param(0) == "CONVERGED")
      {
        Nx = -1;
        f_max_converged = to_num<Real_wp>(param(1));
      }
      else
      {
        Nx = to_num<Real_wp>(param(0));
      }
    }

    else if (keyword == "AuxiliaryConstantString")
    {
      auxiliary_constant = to_num<Real_wp>(param(0));
    }


    else if (keyword == "ThetaScheme")
    {
      if ((param.GetM() > 2) || (param.GetM() == 0))
      {
        std::cout << "In SetInputData of String" << std::endl;
        std::cout << "ThetaScheme needs one or two parameters, for instance :" << std::endl;
        std::cout << "ThetaScheme = 0.25" << std::endl;
        std::cout << "ThetaScheme = 0.25 0.25" << std::endl;
        std::cout << "Current parameters are : " << std::endl << param << std::endl;
        abort();
      }
      if (param.GetM() == 1)
      {
        theta_fast = to_num<Real_wp>(param(0));
        theta_slow = theta_fast;
      }
      else
      {
        theta_slow = to_num<Real_wp>(param(0));
        theta_fast = to_num<Real_wp>(param(1));
      }
    }
   
    else if (keyword == "SismosString")
    {
      if (param.GetM() <= 1)
      {
        std::cout << "In SetInputData of String" << std::endl;
        std::cout << "Sismos needs at least two parameter, for instance :" << std::endl;
        std::cout << "Sismos = REGULAR Nx_output" << std::endl;
        std::cout << "Sismos = POINTS observation_points" << std::endl;
        std::cout << "Sismos = DDLS" << std::endl;
        std::cout << "Current parameters are : " << std::endl << param << std::endl;
        abort();
      }

      if (param(0) == "REGULAR")
      {
        // Interpolation on a regular grid with Nx_output points
        Nx_output = to_num<int>(param(1));
      }
      else if (param(0) == "POINTS")
      {
        // Interpolation on the specified points
        for (unsigned j = 1; j < param.GetM(); ++j)
        {
          output_points.PushBack(to_num<Real_wp>(param(j)));
        }
      }
      else if (param(0) == "DDLS")
      {
        // No interpolation, outputs on all the Ddls of the mesh
        output_on_ddls = true;
      }
      else if (param(0) == "NODES")
      {
        // No interpolation, outputs on all the nodes of the mesh
        output_on_nodes = true;
      }
    }
    else if (keyword == "OutputFemMatricesString")
    {
      if (param(0) == "YES")
      {
        output_fem_matrices = true;
      }
    }

    else if (keyword == "QuadraticStabilisation")
    {
      if (param.GetM() == 4)
      {
        model->alpha_stab(0) = to_num<Real_wp>(param(0));
        model->alpha_stab(1) = to_num<Real_wp>(param(1));
        model->alpha_stab(2) = to_num<Real_wp>(param(2));
        model->alpha_stab(3) = to_num<Real_wp>(param(3));
      }
      else if (param.GetM() == 2)
      {
        model->alpha_stab(0) = to_num<Real_wp>(param(0));
        model->alpha_stab(2) = to_num<Real_wp>(param(1));
      }
      else if (param(0) == "Factorized")
      {
        model->alpha_stab(0) = model->tension / model->young_mod / model->section;
        model->alpha_stab(2) = model->tension / model->young_mod / model->section;
      }
      else if (param(0) == "Linearized")
      {
        model->alpha_stab(0) = model->tension / model->young_mod / model->section;
        model->alpha_stab(2) = 1.0;
      }
      else
      {
        std::cout << "In SetInputData of String" << std::endl;
        std::cout << "QuadraticStabilisation needs some parameters, for instance :" << std::endl;
        std::cout << "QuadraticStabilisation = 1 0 1e-3 0" << std::endl;
        std::cout << "QuadraticStabilisation = 1 1e-3" << std::endl;
        std::cout << "QuadraticStabilisation = Keyword in (Factorized, Linearized)" << std::endl;
        std::cout << "Current parameters are : " << std::endl << param << std::endl;
        abort();
      }
    }
  }




  void
  PianoString
  ::DisplayInit(std::ostream& out) const
  {
    out << "" << std::endl;
    out << " *-----------------------------------*" << std::endl;
    out << " *         STRING PARAMETERS         *" << std::endl;
    out << " *-----------------------------*-----*" << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    if (model->type == model->LIN)
      out << " | String Model                | " << "Linear 1-dimensional"
                                                  << std::endl;
    else if (model->type == model->StiffLIN)
      out << " | String Model                | " << "Linear 2-dimensional string"
                                                  << std::endl;
    else if (model->type == model->NL)
      out << " | String Model                | " << "Non-Linear 2-dimensional string"
                                                  << std::endl;
    else if (model->type == model->StiffNL)
      out << " | String Model                | " << "Non-Linear 3-dimensional string"
                                                  << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Length (m)                  | " << model->length << std::endl;
    out << " | Section (m²)                | " << model->section << std::endl;
    out << " | Density (kg/m³)             | " << model->density << std::endl;
    out << " | Constraint (N)              | " << model->tension << std::endl;
    out << " | Young modulus (Pa)          | " << model->young_mod << std::endl;
    out << " | Quadratic momentum (m⁴)     | " << model->quad_mod << std::endl;
    out << " | Sheer modulus (Pa)          | " << model->shear_mod << std::endl;
    out << " | Timoshenko coefficient      | " << model->kappa << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Dry Damping (u)             | " << model->dampR(0) << std::endl;
    out << " | Dry Damping (v)             | " << model->dampR(1) << std::endl;
    out << " | Dry Damping (phi)           | " << model->dampR(2) << std::endl;
    out << " | Viscous Damping (u)         | " << model->dampH(0) << std::endl;
    out << " | Viscous Damping (v)         | " << model->dampH(1) << std::endl;
    out << " | Viscous Damping (phi)       | " << model->dampH(2) << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << "" << std::endl;
    out << " *------------------------------------*" << std::endl;
    out << " *        COMPUTED INFOMATIONS        *" << std::endl;
    out << " *-----------------------------*------*" << std::endl;
    const Real_wp speed_u = sqrt(model->tension/(model->density*model->section));
    const Real_wp speed_v = sqrt(model->young_mod/model->density);
    const Real_wp f0_trans = speed_u / (2.0*model->length);
    const Real_wp f0_longi = speed_v / (2.0*model->length);
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Transversal frequency (Hz)  | " << f0_trans << std::endl;
    out << " | Longitudinal frequency (Hz) | " << f0_longi << std::endl;
    out << " | Transversal celerity (m/s)  | " << speed_u << std::endl;
    out << " | Longitudinal celerity (m/s) | " << speed_v << std::endl;
    out << " | Celerity Ratio              | " << speed_v / speed_u << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << "" << std::endl;
    out << " *------------------------------------*" << std::endl;
    out << " *        SPACE DISCRETISATION        *" << std::endl;
    out << " *-----------------------------*------*" << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Nx                          | " << Nx << std::endl;
    out << " | Order                       | " << order << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << "" << std::endl;
    out << " *------------------------------------*" << std::endl;
    out << " *         TIME DISCRETISATION        *" << std::endl;
    out << " *-----------------------------*------*" << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Scheme                      | " << "P-SAV" << std::endl;
    out << " | Auxiliary constant          | " << auxiliary_constant << std::endl;
    out << " | Stabilisation               | " << model->alpha_stab << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << "" << std::endl;
    out << "" << std::endl;
  }



  void
  PianoString
  ::WriteJsonSummary(JsonManager* jsm) const
  {
    JsonCategory* js_string = jsm->AddCategory("String");
    js_string->AddMember("Length",                  to_str(model->length));
    //js_string->AddMember("Radius",                  to_str(shank.radius));
    js_string->AddMember("Section",                 to_str(model->section));
    js_string->AddMember("Density",                 to_str(model->density));
    js_string->AddMember("Young modulus",           to_str(model->young_mod));
    js_string->AddMember("Quadratic momentum",      to_str(model->quad_mod));
    js_string->AddMember("Sheer modulus",           to_str(model->shear_mod));
    js_string->AddMember("Timoshenko coefficient",  to_str(model->kappa));
    js_string->AddMember("Dry Damping Trans",       to_str(model->dampR(0)));
    js_string->AddMember("Dry Damping Longi",       to_str(model->dampR(1)));
    js_string->AddMember("Dry Damping Shear",       to_str(model->dampR(2)));
    js_string->AddMember("Viscous Damping Trans",   to_str(model->dampH(0)));
    js_string->AddMember("Viscous Damping Longi",   to_str(model->dampH(1)));
    js_string->AddMember("Viscous Damping Shear",   to_str(model->dampH(2)));

    js_string->AddMember("Nx",                      to_str(Nx));
    js_string->AddMember("Order",                   to_str(order));
    js_string->AddMember("Auxiliary constant",      to_str(auxiliary_constant));

    js_string->AddMember("Model dimension",         to_str(model->dimension));
    if (model->dimension == 1)
    {
      js_string->AddMember("Model type",            "\"LIN\"");
    }
    else if (model->dimension == 2)
    {
      if (model->linear)
      {
        js_string->AddMember("Model type",          "\"StiffLIN\"");
      }
      else
      {
        js_string->AddMember("Model type",          "\"NL\"");
      }
    }
    else if (model->dimension == 3)
    {
      js_string->AddMember("Model type",            "\"StiffNL\"");
    }
  }



  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  //    MAIN PROGRAMS
  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-



  void
  PianoString
  ::Init()
  {
    model->InitEquation();

    if (Nx == -1) // NxString = CONVERGED in .ini file
    {
      const Real_wp speed_u = sqrt(model->tension/(model->density*model->section));
      const Real_wp lambda_min = speed_u / f_max_converged;

      // We want 10 points per wavelength
      Nx = unsigned( model->length * 10 / lambda_min / order );
    }

    Discretize(model->length, Nx, order);
    AssembleFemMatrix(*model);

    InitBuffer();

    Q_minus.Reallocate(shape);      Q_minus.Zero();
    Q.Reallocate(shape);            Q.Zero();
    Q_plus.Reallocate(shape);       Q_plus.Zero();
    Q_tilde.Reallocate(shape);      Q_tilde.Zero();

    Q_dot_minus.Reallocate(shape);  Q_dot_minus.Zero();
    Q_dot.Reallocate(shape);        Q_dot.Zero();
    Q_dot_plus.Reallocate(shape);   Q_dot_plus.Zero();

    Z_minus = sqrt(auxiliary_constant);
    Z_plus = sqrt(auxiliary_constant);
  }



  void
  PianoString
  ::ChooseTimeStepCFL(Real_wp &dt_piano)
  {
    abort();
  }



  void
  PianoString
  ::ComputeConstDiffMatrix(srmatrix &M) const
  {
    srmatrix temp(shape, shape);
    Add(Real_wp(2.0)/dt, Mh, temp);
    Add(Real_wp(0.5)*dt, Kh, temp);
    if (model->damped)
    {
      Add(Real_wp(1.0), Rh, temp);
    }

    for (unsigned i = 0; i < shape; ++i)
    {
      for (unsigned j = 0; j < shape; ++j)
      {
        if (temp(i,j) != 0.0)
        {
          M.AddInteraction(offset+i, offset+j, temp(i,j));
        }
      }
    }

    M.AddInteraction(offset+shape, offset+shape, 2.0/dt);
  }



  void
  PianoString
  ::ComputeDiffMatrix(srmatrix &M) const
  {
    //Vector<int> range(shape);
    //for (unsigned i = 0; i < shape; i++){ range(i) = offset+i; }
    //M.AddInteractionRow(int(offset+shape), int(shape), range, SAVvect);

    //rvector SAVvect_neg(shape); 
    //Copy(SAVvect, SAVvect_neg); 
    //Mlt(Real_wp(-1), SAVvect_neg);
    //M.AddInteractionColumn(i_vec, size, range, SAVvect_neg);

    for (unsigned i = 0; i < shape; ++i)
    {
      M.AddInteraction(offset+i,     offset+shape,  SAVvect(i));
      M.AddInteraction(offset+shape, offset+i    , -SAVvect(i));
    }
  }




  void
  PianoString
  ::OneStep(srmatrix &M, rvector &RHS)
  {
    SAVvect = AssembleSAV(Q_tilde, auxiliary_constant, *model);
    ComputeRightHandSide(RHS);
    ComputeDiffMatrix(M);
  }




  void
  PianoString
  ::ExtractNewSol(const rvector &SOL)
  {
    // Wathch out, SOL contains muP and muZ
    for (unsigned i = 0; i < shape; ++i) 
    {
      Q_dot_plus(i) = 2.0*SOL(offset+i) - Q_dot(i);
      Q_plus(i) = dt*SOL(offset+i) + Q(i);
    }
    Z_plus = 2.0*SOL(offset+shape) - Z_minus;
  }




  void
  PianoString
  ::UpdateValues()
  {
    Q_minus = Q;
    Q = Q_plus;
    Q_tilde = Real_wp(0.5)*(Real_wp(3.0)*Q - Q_minus);

    Q_dot_minus = Q_dot;
    Q_dot = Q_dot_plus;

    Z_minus = Z_plus;
  }



  void
  PianoString
  ::ApplyBoundaryConditions(srmatrix &M) const
  {
    Real_wp mem;
    // Boundary conditions at x=0
    for (unsigned k = 0; k < model->dimension; ++k)
    {
      if (model->dirichlet_0(k))
      {
        mem = M.Get(offset+k,offset+k);
        M.ClearRow(offset+k);
        M.AddInteraction(offset+k, offset+k, mem);
      }
    }

    /*// Boundary conditions at x=L
    for (int k = 0; k < model->dimension; k++)
    {
      if (model->dirichlet_L(k))
      {
        mem = M.Get(offset+shape-model->dimension+k, 
                    offset+shape-model->dimension+k);
        M.ClearRow(offset + shape - model->dimension + k);
        M.AddInteraction(offset+shape-model->dimension+k, 
                          offset+shape-model->dimension+k, mem);
      }
    }*/
  }




  void
  PianoString
  ::ComputeRightHandSide(rvector &RHS) const
  {
    rvector temp(shape); temp.Zero();

    MltAdd(Real_wp(2.0)/dt, Mh, Q_dot, Real_wp(1.0), temp);
    MltAdd(Real_wp(-1.0), Kh, Q, Real_wp(1.0), temp);

    for (unsigned i = 0; i < shape; ++i)
    {
      RHS(offset+i) = temp(i);
    }

    RHS(offset+shape) = 2.0/dt * Z_minus;

    for (unsigned k = 0; k < model->dimension; ++k)
    {
      if (model->dirichlet_0(k))
      {
        RHS(offset + k) = 0.0;
      }
      /*if (model->dirichlet_L(k))
      {
        RHS(offset + shape - model->dimension + k) = 0.0;
      }*/
    }
  }



  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  //    ENERGY
  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-



  Real_wp
  PianoString
  ::ComputeKineticEnergy() const
  {
    rvector res(shape);

    Mlt(Mh, Q_dot_plus, res);
    return 0.5*DotProd(res, Q_dot_plus);

  }


  Real_wp
  PianoString
  ::ComputePotentialEnergy() const
  {
    rvector res(shape);

    Mlt(Kh, Q_plus, res);
    return 0.5*DotProd(res, Q_plus);
  }


  Real_wp
  PianoString
  ::ComputeNonLinearEnergy() const
  {
    return 0.5*Z_plus*Z_plus;
  }


  Real_wp
  PianoString
  ::ComputeDissipatedEnergy() const
  {
    Real_wp dissip_energy = 0.0;

    if (model->damped)
    {
      Vector<Real_wp> temp(shape), res(shape);

      temp = Real_wp(0.5)*(Q_dot_plus + Q_dot);

      Mlt(Rh, temp, res);
      dissip_energy -= DotProd(res, temp);
    }

    return dissip_energy;
  }



  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  //    OUTPUTS
  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-



  void
  PianoString
  ::InitBuffer()
  {
    if (output_on_ddls)
    {
      outputs_string.Reallocate(shape);
    }
    else if (output_on_nodes)
    {
      outputs_string.Reallocate(Nx);
    }
    else
    {
      outputs_string.Reallocate(Nx_output);
    }
    for (unsigned p = 0; p < outputs_string.GetM(); ++p)
    {
      outputs_string(p).SetDoublePrecision();
      outputs_string(p).Init(file_base + file_output_string
                              + "_Point" + to_str(p) + ".txt",
                              size_buffer);
    }

    if (interp_grad)
    {
      if (output_on_ddls)
      {
        outputs_string_grad.Reallocate(shape);
      }
      else if (output_on_nodes)
      {
        outputs_string_grad.Reallocate(Nx);
      }
      else
      {
        outputs_string_grad.Reallocate(Nx_output);
      }
      for (unsigned p = 0; p < outputs_string_grad.GetM(); ++p)
      {
        outputs_string_grad(p).SetDoublePrecision();
        outputs_string_grad(p).Init(file_base + file_output_string_grad
                                      + "_Point" + to_str(p) + ".txt",
                                      size_buffer);
      }
    }

    if (output_aux_var)
    {
      output_auxiliary.Reallocate(1); // Will be changed...
      output_auxiliary(0).SetDoublePrecision();
      output_auxiliary(0).Init(file_base + file_output_aux_var + ".txt",
                                    size_buffer);
    }

    interp_values.Reallocate(model->dimension);
    interp_values_plus.Reallocate(model->dimension);
    if (interp_grad)
    {
      interp_values_grad.Reallocate(model->dimension);
      interp_values_grad_plus.Reallocate(model->dimension);
    }
  }




  Vector<rvector>
  PianoString
  ::ExtractCompsFromQ(const rvector &X) const
  {
    Vector<rvector> res(model->dimension);
    for (unsigned k = 0; k < model->dimension; ++k)
    {
      res(k).Reallocate(nH1);
      for (unsigned p = 0; p < nH1; ++p)
      {
        res(k)(p) = X(k + model->dimension*p);
      }
    }
    return res;
  }




  void
  PianoString
  ::WriteSismos(const DiscretizedBeam &grid_interp_time)
  {
    bool already_computed = false; // do not recompute the interp etc if there are 2 output points during one single iteration
    for (unsigned l = 0; l < grid_interp_time.Nx_output; ++l)
    {
      const unsigned ne_loc = grid_interp_time.output_loc_on_mesh(l);
      if (ne_loc == current_iter)
      {
        const Real_wp t = grid_interp_time.output_coord_on_mesh(l);
        const Real_wp current_time_output = dt_outputs*l;

        if (!already_computed)
        {
          if (output_on_ddls)
          {
            interp_values = ExtractCompsFromQ(Q);
            interp_values_plus = ExtractCompsFromQ(Q_plus);
            if (interp_grad)
            {
              std::cout << "\033(31mERROR: no gradient output "
                   << "implemented for Ddls.\033(0m" << std::endl;
              abort();
            }
          }
          else if (output_on_nodes)
          {
            const Vector<rvector> WPhi = ExtractCompsFromQ(Q);
            const Vector<rvector> WPhi_plus = ExtractCompsFromQ(Q_plus);
            for (unsigned k = 0; k < model->dimension; ++k)
            {
              interp_values(k).Reallocate(Nx);
              interp_values_plus(k).Reallocate(Nx);
              for (unsigned p = 0; p < Nx; ++p)
              {
                interp_values(k)(p) = WPhi(k)(order*p);
                interp_values_plus(k)(p) = WPhi_plus(k)(order*p);
              }
            }
            if (interp_grad)
            {
              std::cout << "\033(31mERROR: no gradient output "
                   << "implemented for nodes.\033(0m" << std::endl;
              abort();
            }
          }
          else
          {
            const Vector<rvector> UVPhi = ExtractCompsFromQ(Q);
            const Vector<rvector> UVPhi_plus = ExtractCompsFromQ(Q_plus);
            for (unsigned k = 0; k < model->dimension; ++k)
            {
              interp_values(k).Reallocate(Nx_output);
              interp_values_plus(k).Reallocate(Nx_output);
              Mlt(interp_H1, UVPhi(k), interp_values(k));
              Mlt(interp_H1, UVPhi_plus(k), interp_values_plus(k));
              if (interp_grad)
              {
                interp_values_grad(k).Reallocate(Nx_output);
                interp_values_grad_plus(k).Reallocate(Nx_output);
                Mlt(grad_interp_H1, UVPhi(k), interp_values_grad(k));
                Mlt(grad_interp_H1, UVPhi_plus(k), interp_values_grad_plus(k));
              }
            }
          }
          already_computed = true;
        }

        rvector temp(model->dimension + 1);
        temp(0) = current_time_output;

        for (unsigned p = 0; p < interp_values(0).GetM(); ++p)
        {
          for (unsigned k = 0; k < model->dimension; ++k)
          {
            temp(k+1) = interp_values(k)(p)*(1.0-t)
                        + interp_values_plus(k)(p)*t;
          }
          outputs_string(p).AddVect(temp);
        }

        if (interp_grad)
        {
          for (unsigned p = 0; p < interp_values_grad(0).GetM(); ++p)
          {
            for (unsigned k = 0; k < model->dimension; ++k)
            {
              temp(k+1) = interp_values_grad(k)(p)*(1.0-t)
                          + interp_values_grad_plus(k)(p)*t;
            }
            outputs_string_grad(p).AddVect(temp);
          }
        }

        if (output_aux_var)
        {
          rvector temp(2);
          temp(0) = current_time_output;
          temp(1)= Z_minus*(1.0-t) + Z_plus*t;
          output_auxiliary(0).AddVect(temp);
        }
      }
    }
  }



} // namespace