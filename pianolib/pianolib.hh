/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "basics.hxx"

#include "common/seldon_addons.hxx"
#include "common/functions.hxx"
#include "common/outputs.hxx"
#include "common/outputs.cxx"
#include "common/inputs.hxx"
#include "common/inputs.cxx"

#include "common/beam_equations.hxx"
#include "shank/shank_equation.hxx"
#include "hammer/hammer_equation.hxx"

#include "common/discretized_beam.hxx"
#include "common/discretized_beam.cxx"
#include "shank/discretized_shank.hxx"
#include "shank/discretized_shank.cxx"
#include "string/discretized_string.hxx"
#include "string/discretized_string.cxx"

#include "shank/shank.hxx"
#include "shank/shank.cxx"
#include "string/string.hxx"
#include "string/string.cxx"

#include "piano.hxx"
#include "piano.cxx"

#include "couplings/hammer_string.cxx"
#include "couplings/bridge.cxx"