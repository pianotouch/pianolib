/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{


  //template <class T>
  //std::ostream& operator<<(std::ostream &out, const std::vector<T> &V);

  //void WriteTxt(const std::string &data_file, const srmatrix &M);


  class Timer
  { // Must be improved, I think the timer itself takes too much time ... <sys/time.h> ?
  public:
    double duration;
    std::chrono::time_point<std::chrono::steady_clock> start, stop;
    Timer() { duration = 0; };
    inline double time_to_float(std::chrono::duration<double> &time) const {
      return std::chrono::duration_cast<std::chrono::duration<double>>(time).count();
    }
    inline void Start() { start = std::chrono::steady_clock::now(); };
    inline void Stop() {
      stop = std::chrono::steady_clock::now();
      std::chrono::duration<double> diff = stop - start;
      duration += time_to_float(diff);
    }
    inline double GetSeconds() const { return duration; };
  };


  class WriteOnTheGoWithBuffer
  {
  protected:
    unsigned size_buffer;
    unsigned iter;
    Vector<rvector> buffer;
    std::string name_file;
    bool double_precision;
    
  public:
    WriteOnTheGoWithBuffer();
    void Init(const std::string &data_file, const unsigned &size);
    void AddVect(const rvector &data);
    inline void SetDoublePrecision(){ double_precision = true; };
    inline void SetSinglePrecision(){ double_precision = false; };
    void CloseBuffer();
    ~WriteOnTheGoWithBuffer();
  };


  class JsonSubCategory
  {
  public:
    std::string name;
    std::map<std::string, std::string> members;

    JsonSubCategory(const std::string &category_name) : name(category_name) {}

    inline void AddMember(const std::string &key, const std::string &value)
    {
      members[key] = value;
    }
  };


  class JsonCategory
  {
  public:
    std::string name;
    std::map<std::string, std::string> members;
    std::vector<JsonSubCategory*> subcategories_;

    JsonCategory(const std::string &category_name) : name(category_name) {}

    inline void AddMember(const std::string &key, const std::string &value)
    {
      members[key] = value;
    }

    JsonSubCategory* AddSubCategory(const std::string &category_name)
    {
      subcategories_.push_back(new JsonSubCategory(category_name));
      return subcategories_.back();
    }
  };


  class JsonManager : public JsonCategory
  {
  public:
    std::string filename_;
    std::vector<JsonCategory*> categories_;

    JsonManager(const std::string &filename)
      : JsonCategory("base"), filename_(filename)
    {
      categories_.push_back(this);
    }

    JsonCategory* AddCategory(const std::string &category_name)
    {
      categories_.push_back(new JsonCategory(category_name));
      return categories_.back();
    }

    void Write();
  };


} // namespace