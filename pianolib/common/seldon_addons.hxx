#pragma once

namespace Seldon
{

  template<class T> inline T square(const T &x){ return x*x; };


  // Additional complex-real mixings
  template <class T,
      class Prop1, class Storage1, class Allocator1,
      class Prop2, class Storage2, class Allocator2>
  inline void Add(const T& alpha,
                    const Matrix<T, Prop1, Storage1, Allocator1>& A,
                          Matrix<complex<T>, Prop2, Storage2, Allocator2>& B)
  {
    AddMatrix(alpha, A, B);
  }


  template <class T, class Prop0, class Storage0, class Allocator0,
                                  class Storage1, class Allocator1,
                                  class Storage2, class Allocator2>
  inline void Mlt(const Matrix<T, Prop0, Storage0, Allocator0>& M,
                    const Vector<T, Storage1, Allocator1>& X,
                          Vector<complex<T>, Storage2, Allocator2>& Y)
  {
    MltVector(M, X, Y);
  }


  template<class T, class Storage1, class Allocator1,
                    class Storage2, class Allocator2>
  inline void Add(const T& alpha,
                    const Vector<T, Storage1, Allocator1>& X,
                          Vector<complex<T>, Storage2, Allocator2>& Y)
  {
    AddVector(alpha, X, Y);
  }


  template<class T, class Storage1, class Allocator1,
                    class Storage2, class Allocator2>
  inline void Add(const complex<T>& alpha,
                    const Vector<T, Storage1, Allocator1>& X,
                          Vector<complex<T>, Storage2, Allocator2>& Y)
  {
    AddVector(alpha, X, Y);
  }


  //! returns maximum absolute value of X
  /*!
    For complex numbers, we use |z| = |Re(z)| + |Im(z)|
    so that the function is the same as Blas equivalent dzasum
  */
  template<class T1, class Storage1, class Allocator1>
  typename ClassComplexType<T1>::Treal
  NormInf(const Vector<T1, Storage1, Allocator1>& X)
  {
    typename ClassComplexType<T1>::Treal value(0), temp;
    for (long i = 0; i < X.GetM(); ++i)
    {
      temp = value;
      value = max(value, ComplexAbs(X(i)));
    }
    return value;
  }


  //! returns maximum absolute value of X
  /*!
    For complex numbers, we use |z| = |Re(z)| + |Im(z)|
    so that the function is the same as Blas equivalent dzasum
  */
  template<class T1, class Allocator1>
  typename ClassComplexType<T1>::Treal
  NormInf(const Vector<T1, VectSparse, Allocator1>& X)
  {
    typename ClassComplexType<T1>::Treal value(0), temp;
    for (long i = 0; i < X.GetM(); ++i)
    {
      temp = value;
      value = max(value, ComplexAbs(X.Value(i)));
    }
    return value;
  }


} // namespace Seldon