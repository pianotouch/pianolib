/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{

  //! base class to derive in order to read a data file with the function ReadInputFile
  class DataFileReader
  {
  public:
    virtual void SetInputData(const std::string&, const Vector<std::string>&) = 0;
  };

  std::istream& getlineM(std::istream &file_in, std::string &line);

  void StringTokenize(const std::string &chaine, 
                        Vector<std::string> &param, 
                        const std::string &delim="");
  
  void ReadLinesFile(const std::string &file_name, 
                        Vector<std::string> &all_lines);
  
  void ReadInputFile(const std::string &file_name, 
                        DataFileReader &vars);
  void ReadInputFile(const Vector<std::string> &all_lines, 
                        DataFileReader &vars);


} // namespace