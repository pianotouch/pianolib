/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{

  class DiscretizedBeam
  {

  public:
    DiscretizedBeam();


    Real_wp beam_length; // Length of the beam which is discretized by this class


    // Mesh stuff
    //-----------
    unsigned Nx; // Number of nodes
    rvector nodes; // The nodes of the mesh
    Real_wp dx; // Size of the elements (constant)

    void Discretize(const Real_wp &_beam_length, 
                      const unsigned &number_elements, 
                      const unsigned &_order);
    void ConstructMesh();
    void ConstructNumbering();


    // Finite elements stuff
    //----------------------
    unsigned order; // Order of the FEM and for the GL quadrature
    unsigned nb_base; // Number of basis functions (=order+1)

    rvector factors_phi; // Coefs used to compute the values of the basis functions
    rvector quad_weights, quad_points; // The weights and points of the GL quadrature of [0,1]

    rmatrix valdPhi; // The values of the gradient of the basis functions at all the dofs of the mesh

    unsigned nL2, nH1; // Number of dofs with L2 and H1 numbering (discontinuous vs continuous)
    Matrix<unsigned> dof_num_L2, dof_num_H1; // Global/Local numbering of the dofs

    /**
     * Computes the Gauss-Lobatto quadrature points and weights on the 
     * interval [0,1]
     * 
     * @param[in] r the order of the quadrature
     * @param[out] pts the points of the quadrature
     * @param[out] w the weights of the quadrature
     */
    void ConstructLobattoQuad(const unsigned &r, rvector &pts, 
                                                        rvector &wts);

    /**
     * Computes the coefficients of the Lagrange polynomials.
     *
     * @param[out] lagrange_coefs.
     */
    void ComputeLagrangeCoefs(rvector &lagrange_coefs);

    /**
     * Evaluates a finite element basis function.
     *
     * @param num the rank of the basis function.
     * @param point the Real_wp value at which the function is evaluated.
     * @return the value of the num-th basis function at x=point.
     */
    Real_wp EvaluateBasisFunction(const unsigned &num, const Real_wp &point) const;

    /**
     * Evaluates the gradient of a finite element basis function.
     *
     * @param num the rank of the basis function.
     * @param point the Real_wp value at which the function is evaluated.
     * @return the value of the gradient of the num-th basis function at x=point.
     */
    Real_wp EvaluateGradBasisFunction(const unsigned &num, const Real_wp &point) const;

    void ComputeGradPhi();

    sym_srmatrix AssembleMassMatrix(const rmatrix &M, const bool &use_H1=true) const;
    srmatrix AssembleSemiGradMatrix(const rmatrix &M, const bool &use_H1=true) const;
    srmatrix AssembleStiffnessMatrix(const rmatrix &M, const bool &use_H1=true) const;


    // Output stuff
    //-------------

    unsigned Nx_output; // Number of output points to write on output files
    rvector output_points; // The actual output points
    Vector<int> output_loc_on_mesh; // output_loc_on_mesh(i) is the element of the mesh in which the i-th output point is located
    rvector output_coord_on_mesh; // output_coord_on_mesh(i) is the position (in [0,1]) of the output point in the corresponding element of the mesh

    srmatrix interp_H1, interp_L2; // Interpolation matrix
    srmatrix grad_interp_H1, grad_interp_L2; // Interpolation matrix for the gradient

    void ConstructOutputGrid();
    void LocalizeOutputPoints();
    void ConstructInterpolationMatrix();


  };

} // namespace