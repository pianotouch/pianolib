/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{

  class GenericBeamEquation
  {
  public:
    int type; enum {LIN, StiffLIN, NL, StiffNL};

    Real_wp length;
    Real_wp section;
    Real_wp radius;
    Real_wp density;
    Real_wp tension;
    Real_wp young_mod;
    Real_wp quad_mod; // Quadratic momentum (m^4)
    Real_wp shear_mod;
    Real_wp kappa; // Timoshenko coefficient

    rvector dampR; // dry damping
    rvector dampH; // viscous damping

    rmatrix M, B, C, R, H;
    rmatrix Astab, Aslow, Afast, A;

    rvector alpha_stab;

    Vector<bool> dirichlet_0;
    Vector<bool> dirichlet_L;

    unsigned dimension, nb_LM;
    bool linear;
    bool stiff;
    bool damped;

    GenericBeamEquation()
    {
      length = 1.0;
      section = 9.7993e-7;
      density = 7850;
      tension = 880;
      young_mod = 2.02e11;
      quad_mod = 7.6416e-14;
      shear_mod = 7.77e10;
      kappa = 0.85;
      dampR.Reallocate(3); dampR.Zero();
      dampH.Reallocate(3); dampH.Zero();
      damped = false;

      alpha_stab.Reallocate(4);
      alpha_stab(0) = tension/young_mod/section;
      alpha_stab(1) = 1.0;
      alpha_stab(2) = 1.0;
      alpha_stab(3) = 1.0;
    }

    virtual inline void InitializeObject(){};

    virtual inline void InitEquation(){};

    virtual inline void
    ComputeU(const rvector &q, Real_wp &res) const {};

    virtual inline void
    ComputeGradU(const rvector &q, rvector &res) const {};

    virtual inline void
    ComputeUtilde(const rvector &q, Real_wp &res) const {};

    virtual inline void
    ComputeGradUtilde(const rvector &q, rvector &res) const {};

    virtual inline void
    AuxFunctionIEQ(const rvector &q, rvector &res,
                      const Real_wp &auxiliary_constant) const {};

    virtual inline void
    DiffUtilde(const rvector &q_plus, const rvector &q_minus,
                  rvector &res) const {};

    virtual inline void
    DiffDiffUtilde(const rvector &q_plus, const rvector &q_minus,
                      rmatrix &res) const {};

  };



  class LinearBeam : public GenericBeamEquation
  {
    // ***************************
    // Linear one dimensional beam
    // ***************************

    //! D'Alembert equation (unknown: u)
    /*
      density A \frac{\partial^2 u}{\partial t^2} - tension \frac{\partial^2 u}{\partial x^2} = 0
    */

  public:

    //! Default constructor
    LinearBeam() : GenericBeamEquation()
    {
      InitializeObject();
    }


    //! Copy constructor
    LinearBeam(const GenericBeamEquation &model) : GenericBeamEquation(model)
    {
      InitializeObject();
    }


    inline void
    InitializeObject()
    {
      type = LIN;
      dimension = 1;
      nb_LM = 1;
      linear = true;
      stiff = false;

      M.Reallocate(dimension, dimension); M.Zero();
      B.Reallocate(dimension, dimension); B.Zero();
      C.Reallocate(dimension, dimension); C.Zero();
      R.Reallocate(dimension, dimension); R.Zero();
      H.Reallocate(dimension, dimension); H.Zero();
      Astab.Reallocate(dimension, dimension); Astab.Zero();
      Aslow.Reallocate(dimension, dimension); Aslow.Zero();
      Afast.Reallocate(dimension, dimension); Afast.Zero();

      dirichlet_0.Reallocate(dimension);
      dirichlet_L.Reallocate(dimension);
      dirichlet_0(0) = true;
      dirichlet_L(0) = true;
    }


    inline void
    InitEquation()
    {
      M(0,0) = density*section;
      R(0,0) = 2.0*density*section*dampR(0);
      H(0,0) = 2.0*tension*dampH(0);
      Astab(0,0) = 0;
      Aslow(0,0) = tension;
      Afast(0,0) = 0;
      B(0,0) = 0;
      C(0,0) = 0;

      if ((dampR(0) != 0.0) || (dampH(0) != 0.0))
      {
        damped = true;
      }
    }

    //! Compute U(q)
    inline void
    ComputeU(const rvector &q, Real_wp &res) const
    {
      res = 0;
    }

    //! Compute gradU(q)
    inline void
    ComputeGradU(const rvector &q, rvector &res) const
    {
      res.Zero();
    }

    //! Compute Utilde(q)
    inline void
    ComputeUtilde(const rvector &q, Real_wp &res) const
    {
      res = 0;
    }

    //! Compute gradUtilde(q)
    inline void
    ComputeGradUtilde(const rvector &q, rvector &res) const
    {
      res.Zero();
    }

    //! Compute IEQ auxiliary function
    inline void
    AuxFunctionIEQ(const rvector &q, rvector &res,
                      const Real_wp &auxiliary_constant) const
    {
      res.Zero();
    }
  };



  class StiffLinearBeam : public GenericBeamEquation
  {
    // *********************************
    // Linear stiff two dimensional beam
    // *********************************

    //! Timoshenko equations (unknowns: u, phi)
    /*
      density A d^2 u/dt^2 - (A G K + tension) d^2 u/dx^2 + A G K dphi/dx = S(x, t)
      density I d^2 phi/dt^2 - E I d^2 phi/dx^2 - A G K du/dx + A G K phi = 0
    */
  public:

    //! Default constructor
    StiffLinearBeam() : GenericBeamEquation()
    {
      InitializeObject();
    }


    //! Copy constructor
    StiffLinearBeam(const GenericBeamEquation &model) : GenericBeamEquation(model)
    {
      InitializeObject();
    }


    inline void
    InitializeObject()
    {
      type = StiffLIN;
      dimension = 2;
      nb_LM = 1;
      linear = true;
      stiff = true;

      M.Reallocate(dimension, dimension); M.Zero();
      B.Reallocate(dimension, dimension); B.Zero();
      C.Reallocate(dimension, dimension); C.Zero();
      R.Reallocate(dimension, dimension); R.Zero();
      H.Reallocate(dimension, dimension); H.Zero();
      Astab.Reallocate(dimension, dimension); Astab.Zero();
      Aslow.Reallocate(dimension, dimension); Aslow.Zero();
      Afast.Reallocate(dimension, dimension); Afast.Zero();

      dirichlet_0.Reallocate(dimension);
      dirichlet_L.Reallocate(dimension);
      dirichlet_0(0) = true;
      dirichlet_0(1) = false;
      dirichlet_L(0) = true;
      dirichlet_L(1) = false;
    }


    inline void
    InitEquation()
    {
      M(0,0) = density*section;
      M(1,1) = density*quad_mod;

      R(0,0) = 2.0*density*section*dampR(0);
      R(1,1) = 2.0*density*quad_mod*dampR(2);

      H(0,0) = 2.0*tension*dampH(0);
      H(1,1) = 2.0*young_mod*quad_mod*dampH(2);

      if ((dampR(0) != 0.0) || (dampR(2) != 0.0)
              || (dampH(0) != 0.0) || (dampH(2) != 0.0))
      {
        damped = true;
      }

      Aslow(0,0) = tension;

      Afast(0,0) = section*shear_mod*kappa;
      Afast(1,1) = young_mod*quad_mod;

      B(0,1) = -section*shear_mod*kappa;

      C(1,1) = section*shear_mod*kappa;
    }

    //! Compute U(q)
    inline void
    ComputeU(const rvector &q, Real_wp &res) const
    {
      res = 0;
    }

    //! Compute gradU(q)
    inline void
    ComputeGradU(const rvector &q, rvector &res) const
    {
      res.Zero();
    }

    //! Compute Utilde(q)
    inline void
    ComputeUtilde(const rvector &q, Real_wp &res) const
    {
      res = 0;
    }

    //! Compute gradUtilde(q)
    inline void
    ComputeGradUtilde(const rvector &q, rvector &res) const
    {
      res.Zero();
    }

    //! Compute IEQ auxiliary function
    inline void
    AuxFunctionIEQ(const rvector &q, rvector &res,
                      const Real_wp &auxiliary_constant) const
    {
      res.Zero();
    }
  };



  class NonLinearBeam : public GenericBeamEquation
  {
    // *****************************************
    // Non-linear non-stiff two dimensional beam
    // *****************************************

    //! Geometrically Exact Model (unknowns: u, v)
    /*
      density A d^2 u/dt^2 - tension d^2 u/dx^2
           - d/dx( (E A - tension) du/dx (1 - 1 / sqrt( (du/dx)^2 + (1 + dv/dv)^2 ) ) ) = 0
      density A d^2 v/dt^2 - E A d^2 v/dx^2
           - d/dx( (E A - tension) (1 - 1 / sqrt( (du/dx)^2 + (1 + dv/dv)^2 ) ) ) = 0
    */
  public:

    //! Default constructor
    NonLinearBeam() : GenericBeamEquation()
    {
      InitializeObject();
    }

    //! Copy constructor
    NonLinearBeam(const GenericBeamEquation &model) : GenericBeamEquation(model)
    {
      InitializeObject();
    }

    inline void
    InitializeObject()
    {
      type = NL;
      dimension = 2;
      nb_LM = 2;
      linear = false;
      stiff = false;

      M.Reallocate(dimension, dimension); M.Zero();
      B.Reallocate(dimension, dimension); B.Zero();
      C.Reallocate(dimension, dimension); C.Zero();
      R.Reallocate(dimension, dimension); R.Zero();
      H.Reallocate(dimension, dimension); H.Zero();
      Astab.Reallocate(dimension, dimension); Astab.Zero();
      Aslow.Reallocate(dimension, dimension); Aslow.Zero();
      Afast.Reallocate(dimension, dimension); Afast.Zero();

      dirichlet_0.Reallocate(dimension);
      dirichlet_L.Reallocate(dimension);
      dirichlet_0(0) = true;
      dirichlet_0(1) = true;
      dirichlet_L(0) = true;
      dirichlet_L(1) = true;
    }

    inline void
    InitEquation()
    {
      M(0,0) = density*section;
      M(1,1) = density*section;

      R(0,0) = 2.0*density*section*dampR(0);
      R(1,1) = 2.0*density*section*dampR(1);

      H(0,0) = 2.0*tension*dampH(0);
      H(1,1) = 2.0*young_mod*section*dampH(1);

      if ((dampR(0) != 0.0) || (dampR(1) != 0.0)
              || (dampH(0) != 0.0) || (dampH(1) != 0.0))
      {
        damped = true;
      }

      Aslow(0,0) = alpha_stab(0)*young_mod*section;
      Afast(1,1) = alpha_stab(2)*young_mod*section;

      Astab(0,0) = (1 - alpha_stab(0))*young_mod*section;
      Astab(1,1) = (1 - alpha_stab(2))*young_mod*section;
    }

    //! Compute U(q)
    inline void
    ComputeU(const rvector &q, Real_wp &res) const
    {
      const Real_wp coef = (tension - young_mod*section);
      const Real_wp racine = sqrt(q(0)*q(0) + (1.0 + q(1))*(1.0 + q(1)));
      res = coef*(racine - (1.0 + q(1)));
    }

    //! Compute gradU(q)
    inline void
    ComputeGradU(const rvector &q, rvector &res) const
    {
      const Real_wp coef = (tension - young_mod*section);
      const Real_wp racine = sqrt(q(0)*q(0) + (1.0 + q(1))*(1.0 + q(1)));
      res(0) = coef*q(0)/racine;
      res(1) = coef*((1.0 + q(1))/racine - 1.0);
    }

    //! Compute Utilde(q)
    inline void
    ComputeUtilde(const rvector &q, Real_wp &res) const
    {
      rvector res1(dimension);
      ComputeU(q, res);
      Mlt(Astab, q, res1);
      res += 0.5 * DotProd(res1, q);
    }

    //! Compute gradUtilde(q)
    inline void
    ComputeGradUtilde(const rvector &q, rvector &res) const
    {
      ComputeGradU(q, res);
      MltAdd(Real_wp(1.0), Astab, q, Real_wp(1.0), res);
    }

    //! Compute IEQ auxiliary function
    inline void
    AuxFunctionIEQ(const rvector &q, rvector &res,
                      const Real_wp &auxiliary_constant) const
    {
      ComputeGradUtilde(q, res);
      Real_wp res1;
      ComputeUtilde(q, res1);
      res = res / sqrt(Real_wp(2.0)*res1 + auxiliary_constant);
    }


    inline void
    DiffUtilde(const rvector &q_plus, const rvector &q_minus, rvector &res) const
    {
      const Real_wp coef = (tension - young_mod*section);
      Real_wp r1 = sqrt(square(q_plus(0)) + square(1.0 + q_minus(1)));
      Real_wp r2 = sqrt(square(q_minus(0)) + square(1.0 + q_minus(1)));
      res(0) = coef * (q_plus(0) + q_minus(0)) / (r1 + r2);

      r1 = sqrt(square(q_plus(0)) + square(1.0 + q_plus(1)));
      r2 = sqrt(square(q_minus(0)) + square(1.0 + q_plus(1)));
      res(0) += coef * (q_plus(0) + q_minus(0)) / (r1 + r2);

      r1 = sqrt(square(q_plus(0)) + square(1.0 + q_plus(1)));
      r2 = sqrt(square(q_plus(0)) + square(1.0 + q_minus(1)));
      res(1) = coef * ((2.0 + q_plus(1) + q_minus(1)) / (r1 + r2) - 1.0);

      r1 = sqrt(square(q_minus(0)) + square(1.0 + q_plus(1)));
      r2 = sqrt(square(q_minus(0)) + square(1.0 + q_minus(1)));
      res(1) += coef * ((2.0 + q_plus(1) + q_minus(1)) / (r1 + r2) - 1.0);

      MltAdd(Real_wp(1.0), Astab, q_plus, Real_wp(1.0), res);
      MltAdd(Real_wp(1.0), Astab, q_minus, Real_wp(1.0), res);
    }


    inline void
    DiffDiffUtilde(const rvector &q_plus, const rvector &q_minus, rmatrix &res) const
    {
      /* d_u d_u & d_v d_u
         d_u d_v & d_v d_v */

      const Real_wp coef = (tension - young_mod*section);

      Real_wp r_plus = sqrt(square(q_plus(0)) + square(1.0 + q_minus(1)));
      Real_wp r_minus = sqrt(square(q_minus(0)) + square(1.0 + q_minus(1)));
      res(0,0) = coef * (1.0/(r_plus + r_minus)
                          - (q_plus(0)+q_minus(0))*q_plus(0)
                          / (r_plus*square((r_plus+r_minus))));

      r_plus = sqrt(square(q_minus(0)) + square(1.0 + q_plus(1)));
      r_minus = sqrt(square(q_minus(0)) + square(1.0 + q_minus(1)));
      res(1,1) = coef * (1.0/(r_plus + r_minus) - (2.0+q_plus(1)+q_minus(1))
                  *(1.0+q_plus(1))/(r_plus*(r_plus+r_minus)*(r_plus+r_minus)));

      r_plus = sqrt(square(q_plus(0)) + square(1.0 + q_plus(1)));
      r_minus = sqrt(square(q_minus(0)) + square(1.0 + q_plus(1)));
      res(0,0) += coef * (1.0/(r_plus + r_minus)
                          - (q_plus(0)+q_minus(0))*q_plus(0)
                          / (r_plus*square((r_plus+r_minus))));

      res(0,1) = -coef * (q_plus(0) + q_minus(0)) * (1.0 + q_plus(1))
                      * (1.0/r_plus + 1.0/r_minus) / square(r_plus+r_minus);

      r_plus = sqrt(square(q_plus(0)) + square(1.0 + q_plus(1)));
      r_minus = sqrt(square(q_plus(0)) + square(1.0 + q_minus(1)));
      res(1,1) += coef * (1.0/(r_plus + r_minus) - (2.0+q_plus(1)+q_minus(1))
                  *(1.0+q_plus(1))/(r_plus*(r_plus+r_minus)*(r_plus+r_minus)));

      res(1,0) = -coef * (2.0+q_plus(1)+q_minus(1)) * q_plus(0)
                      * (1.0/r_plus + 1.0/r_minus) / square(r_plus+r_minus);

      Add(Real_wp(1.0), Astab, res);
    }
  };



  class StiffNonLinearBeam : public GenericBeamEquation
  {
    // ***************************************
    // Non-linear stiff three dimensional beam
    // ***************************************

    //! Most complete model (unknowns: u, v, phi)
    /*
      density A d^2 u/dt^2 - (A G K + tension) d^2 u/dx^2 + A G K dphi/dx
               - d/dx( (E A - tension) du/dx (1 - 1 / sqrt( (du/dx)^2 + (1 + dv/dv)^2 ) ) ) = S(x)
      density A d^2 v/dt^2 - E A d^2 v/dx^2
               - d/dx( (E A - tension) (1 - 1 / sqrt( (du/dx)^2 + (1 + dv/dv)^2 ) ) ) = 0
      density I d^2 phi/dt^2 - E I d^2 phi/dx^2 - A G K du/dx + A G K phi = 0
    */
  public:

    //! Default constructor
    StiffNonLinearBeam() : GenericBeamEquation()
    {
      InitializeObject();
    }

    //! Copy constructor
    StiffNonLinearBeam(const GenericBeamEquation &model) : GenericBeamEquation(model)
    {
      InitializeObject();
    }

    inline void
    InitializeObject()
    {
      type = StiffNL;
      dimension = 3;
      nb_LM = 2;
      linear = false;
      stiff = true;

      M.Reallocate(dimension, dimension); M.Zero();
      B.Reallocate(dimension, dimension); B.Zero();
      C.Reallocate(dimension, dimension); C.Zero();
      R.Reallocate(dimension, dimension); R.Zero();
      H.Reallocate(dimension, dimension); H.Zero();
      Astab.Reallocate(dimension, dimension); Astab.Zero();
      Aslow.Reallocate(dimension, dimension); Aslow.Zero();
      Afast.Reallocate(dimension, dimension); Afast.Zero();

      dirichlet_0.Reallocate(dimension);
      dirichlet_L.Reallocate(dimension);
      dirichlet_0(0) = true;
      dirichlet_0(1) = true;
      dirichlet_0(2) = false;
      dirichlet_L(0) = true;
      dirichlet_L(1) = true;
      dirichlet_L(2) = false;
    }

    inline void
    InitEquation()
    {
      M(0,0) = density*section;
      M(1,1) = density*section;
      M(2,2) = density*quad_mod;

      R(0,0) = 2.0*density*section*dampR(0);
      R(1,1) = 2.0*density*section*dampR(1);
      R(2,2) = 2.0*density*quad_mod*dampR(2);

      H(0,0) = 2.0*tension*dampH(0);
      H(1,1) = 2.0*young_mod*section*dampH(1);
      H(2,2) = 2.0*young_mod*quad_mod*dampH(2);

      if ((dampR(0) != 0.0) || (dampR(1) != 0.0) || (dampR(2) != 0.0)
              || (dampH(0) != 0.0) || (dampH(1) != 0.0) || (dampH(2) != 0.0))
      {
        damped = true;
      }

      Aslow(0,0) = alpha_stab(0)*young_mod*section;
      Afast(1,1) = alpha_stab(2)*young_mod*section;
      Afast(0,0) = alpha_stab(1)*section*shear_mod*kappa;
      Afast(2,2) = alpha_stab(3)*young_mod*quad_mod;

      Astab(0,0) = (1 - alpha_stab(0))*young_mod*section + (1 - alpha_stab(1))*section*shear_mod*kappa;
      Astab(1,1) = (1 - alpha_stab(2))*young_mod*section;
      Astab(2,2) = (1 - alpha_stab(3))*young_mod*quad_mod;


      B(0,2) = -section*shear_mod*kappa;

      C(2,2) = section*shear_mod*kappa;
    }

    //! Compute U(q)
    inline void
    ComputeU(const rvector &q, Real_wp &res) const
    {
      const Real_wp coef = (tension - young_mod*section);
      const Real_wp racine = sqrt(q(0)*q(0) + (1.0 + q(1))*(1.0 + q(1)));
      res = coef*(racine - (1.0 + q(1)));
    }

    //! Compute gradU(q)
    inline void
    ComputeGradU(const rvector &q, rvector &res) const
    {
      const Real_wp coef = (tension - young_mod*section);
      const Real_wp racine = sqrt(q(0)*q(0) + (1.0 + q(1))*(1.0 + q(1)));
      res(0) = coef*q(0)/racine;
      res(1) = coef*((1.0 + q(1))/racine - 1.0);
      res(2) = 0;
    }

    //! Compute Utilde(q)
    inline void
    ComputeUtilde(const rvector &q, Real_wp &res) const
    {
      rvector res1(dimension);
      ComputeU(q, res);
      Mlt(Astab, q, res1);
      res += 0.5 * DotProd(res1, q);
    }

    //! Compute gradUtilde(q)
    inline void
    ComputeGradUtilde(const rvector &q, rvector &res) const
    {
      ComputeGradU(q, res);
      MltAdd(Real_wp(1.0), Astab, q, Real_wp(1.0), res);
    }

    //! Compute IEQ auxiliary function
    inline void
    AuxFunctionIEQ(const rvector &q, rvector &res,
                      const Real_wp &auxiliary_constant) const
    {
      ComputeGradUtilde(q, res);
      Real_wp res1;
      ComputeUtilde(q, res1);
      res = res / sqrt(Real_wp(2.0)*res1 + auxiliary_constant);
    }
  };


} // namespace