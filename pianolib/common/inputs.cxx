/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{

  //! Reads a line by taking care of \r\n problem
  std::istream&
  getlineM(std::istream &file_in, std::string &line)
  {
    // reading a line of the file
    std::getline(file_in, line, '\n'); // std::getline(tokenStream, token, delimiter)
    
    // removing the character 13 to handle dos files
    if (line.size() > 0)
    {
      if (line[line.size()-1] == 13)
      {
        line = line.substr(0, line.size()-1);
      }
    }
    return file_in;
  }



  //! Splits a string into several pieces (=tokens) by taking into account parenthesis
  void
  StringTokenize(const std::string &chaine, 
                    Vector<std::string> &param, 
                    const std::string &delim)
  {
    Vector<std::string> param_;
    unsigned pos = 0;
    int level = 0;
    for (unsigned i = 0; i < chaine.size(); ++i)
    {
      if (chaine[i] == '(')
        level++;

      if (chaine[i] == ')')
        level--;

      if ((delim.find(chaine[i]) != std::string::npos) && (level == 0))
      {
        // a delimiting character has been found => new token
        if (i > pos)
        {
          param_.PushBack(chaine.substr(pos, i-pos));
        }
        pos = i+1;
      }
    }
    
    if (chaine.size() > pos)
    {
      param_.PushBack(chaine.substr(pos, chaine.size()-pos));
    }
    
    param = param_; //Copy(param_, param);
  }
  


  void
  ReadLinesFile(const std::string &file_name, 
                  Vector<std::string> &all_lines)
  {
    // attempt to open the input file
    std::ifstream file_in(file_name.data());
    if (!file_in.is_open())
    {
      std::cerr << "File with datas not found." << std::endl;
      abort();
    }
    
    std::string line;
    all_lines.Clear();
    // reading the file until the end
    while (!file_in.eof())
    {
      getlineM(file_in, line);
      all_lines.PushBack(line);
    }
  }

  
  //! reading of the input file
  /*!
    \param[in] file_name name of the input file
    \param[in,out] vars generic problem to solve
    \return 0 if successful
   */
  void
  ReadInputFile(const std::string &file_name, DataFileReader &vars)
  {
    // we read all the lines
    Vector<std::string> all_lines;
    ReadLinesFile(file_name, all_lines);
    
    // then treating them
    ReadInputFile(all_lines, vars);
  }
  
  
  //! analysing the lines of the data file with the object vars
  void
  ReadInputFile(const Vector<std::string> &all_lines, DataFileReader &vars)
  {
    std::string line;
    std::string description_field;
    Vector<std::string> parameters, words;
   
    // reading the file until the end
    unsigned n = 0;
    while (n < all_lines.GetM())
    {
      line = all_lines(n);
      n++;
        
      // if a line begins with # or %, it is a comment (ignored)
      if ((line.size() > 0) && ((line[0] != '#') || (line[0] != '%')))
      {
        description_field = std::string("");
            
        // the line is split in words
        StringTokenize(line, words, std::string(" \t"));
      
        // first word of the line -> description_field
        if (words.GetM() > 0)
        {
          description_field = words(0);
        }
      
        // all the words after '=' are retrieved and stored in parameters
        // For example, if a line of the input file is equal to
        // GridBoundingBox = -1 1 -1 1
        // description_field will be equal to GridBoundingBox
        // and parameters(0) = -1, parameters(1) = 1 ...
        parameters.Clear();
        if (words.GetM() > 2)
        {
          parameters.Reallocate(words.GetM()-2);
          for (unsigned i = 2; i < words.GetM(); ++i)
            parameters(i-2) = words(i);
          
          unsigned Nw = words.GetM() - 1;
          // if the line ends with \, we look at the following line
          while ((words.GetM() > 0) && (words(Nw)[words(Nw).size()-1] == '\\'))
          {
            unsigned nb_old = parameters.GetM()-1;
            if (words(Nw).size() > 1)
            {
              parameters(nb_old) = words(Nw).substr(0, words(Nw).size()-1);
              nb_old++;
            }
              
            line = all_lines(n); n++;
            StringTokenize(line, words, std::string(" \t"));
            
            parameters.Reallocate(nb_old + words.GetM());
            
            for (unsigned i = 0; i < words.GetM(); ++i)
            {
              parameters(nb_old+i) = words(i);
            }
            
            Nw = words.GetM() - 1;
          }
        }
      
        // keyword and associated values are treated by the
        // method SetInputData that can be overloaded in derived classes
        if (parameters.GetM() > 0)
        {
          vars.SetInputData(description_field, parameters);
        }
      }
    }
  }



} // namespace