/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{

  //! d/dx (sin(x)/x)
  inline Real_wp
  DiffSinCardinal(const Real_wp &x)
  {
    if (abs(x) < 1e-5)
      return -x/Real_wp(3.0);

    return (x*cos(x) - sin(x)) / square(x);
  }



  // (sin(a) - sin(b)) / (a-b)
  inline Real_wp
  DeltaSin(const Real_wp &a, const Real_wp &b)
  {
    Real_wp sin_cardinal = 1.0;
    if (abs(a-b) > 2e-9)
    {
      sin_cardinal = sin(0.5*(a-b)) / (0.5*(a-b));
    }
    return cos(0.5*(a+b)) * sin_cardinal;
  }



  inline Real_wp
  TanhCardinal(const Real_wp &x)
  {
    if (abs(x) < 1e-5)
    {
      return 1.0 - (x*x / 3.0) + (2.0 * pow(x,4) / 15.0);
    }
    else
    {
      return tanh(x) / x;
    }
  }



  // d/da (sin(a) - sin(b)) / (a-b)
  inline Real_wp
  DiffDeltaSin(const Real_wp &a, const Real_wp &b)
  {
    Real_wp sin_cardinal = 1.0;
    if (abs(a-b) > 2e-9)
    {
      sin_cardinal = sin(0.5*(a-b)) / (0.5*(a-b));
    }
    return -0.5 * sin(0.5*(a+b)) * sin_cardinal
            + 0.5 * cos(0.5*(a+b)) * DiffSinCardinal(0.5*(a-b));
  }



  // Function used to distribute a force on a surface area
  inline Real_wp
  Repartition(const Real_wp &x, const Real_wp &slope, const Real_wp &x0, const Real_wp &width)
  {
    return ( 1.0/(1.0 + exp(-slope*(x - x0 + 0.5*width))) 
             -1.0/(1.0 + exp(-slope*(x - x0 - 0.5*width))) ) / width;
  }



  // Gaussian function used to generate input regular sources
  inline Real_wp
  Delta(const Real_wp &t, const Real_wp &t0, const Real_wp &sigma)
  {
    const Real_wp eps = 1e-12;
    if ((t>t0 - sigma + eps) && (t<t0 + sigma - eps))
    {
      return exp(1.0)*exp(-1.0 / (1.0 - square((t - t0)/sigma)));
    }
    else
    {
      return 0.0;
    }
  }



} // namespace