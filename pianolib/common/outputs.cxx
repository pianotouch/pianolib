/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{


  WriteOnTheGoWithBuffer::WriteOnTheGoWithBuffer()
  {
    size_buffer = 100;
    iter = 0;
    double_precision = true;
  }
  

  //! Initialisation of the buffer
  /*!
    \param[in] data_file the destination file of the buffer
    \param[in] size size of the buffer (consecutive values stored before writing them in the file)
    \param[in] remove_file if true, the file is erased before rewriting it,
    otherwise the new values are simply appended at the end of the file
  */  
  void
  WriteOnTheGoWithBuffer
  ::Init(const std::string &data_file, const unsigned &size)
  {
    name_file = data_file;
    std::remove(name_file.data()); // remove existing data in the file if it already exists
    size_buffer = size;
    iter = 0;
    buffer.Reallocate(size_buffer);
  }


  //! Pushes a vector of values into the buffer
  /*!
    When the buffer is full, contents of the buffer are appended to the output file,
    and buffer is emptied, ready for the next push.
  */    
  void
  WriteOnTheGoWithBuffer
  ::AddVect(const rvector &data)
  {
    buffer(iter) = data;
    iter++;
    if (iter == size_buffer)
    {
      std::ofstream file_out(name_file.data(), std::ios::app);
      file_out.setf(std::ios::scientific);

      if (double_precision)
        file_out.precision(15);
      else
        file_out.precision(7);
      
      for (unsigned p = 0; p < size_buffer; ++p)
      {
        file_out << buffer(p);
        file_out << '\n';
      }

      file_out.close(); 
      iter = 0;
    }
  }

  
  void
  WriteOnTheGoWithBuffer
  ::CloseBuffer()
  {
    if (iter == 0)
      return;
    
    std::ofstream file_out(name_file.data(), std::ios::app);
    file_out.setf(std::ios::scientific);

    if (double_precision)
      file_out.precision(15);
    else
      file_out.precision(7);

    for (unsigned p = 0 ; p < iter; ++p)
    {
      file_out << buffer(p);
      file_out << '\n';
    }   
    
    file_out.close(); 
    iter = 0;
  }
  
  
  WriteOnTheGoWithBuffer::~WriteOnTheGoWithBuffer()
  {
    CloseBuffer();
  }


  void JsonManager::Write()
  {
    std::ofstream ofs(filename_);
    if (!ofs.is_open()) {
        std::cerr << "Error: unable to open the json summary file." << std::endl;
        abort();
    }

    ofs << "{" << std::endl;
    for (size_t i = 0; i < categories_.size(); ++i)
    {
      if (i == 0) // base category
      {
        for (const auto& [key, value] : categories_[i]->members)
        {
          ofs << "    \"" << key << "\": " << value << "," << std::endl;
        }
      }
      else
      {
        ofs << "    \"" << categories_[i]->name << "\": {" << std::endl;
        for (auto it = categories_[i]->members.cbegin(); 
                  it != categories_[i]->members.cend(); ++it)
        {
          ofs << "        \"" << it->first << "\": " << it->second;
          if (std::next(it) != categories_[i]->members.cend())
          {
            // If it is not the last attribute we put a comma
            ofs << ",";
          }
          else if (categories_[i]->subcategories_.size() != 0)
          {
            // If it is the last attribute, we only put a comma if there is a following subcategory
            ofs << ",";
          }
          ofs << std::endl;
        }

        for (size_t j = 0; j < categories_[i]->subcategories_.size(); ++j)
        {
          ofs << "        \"" << categories_[i]->subcategories_[j]->name << "\": {" << std::endl;
          for (auto it = categories_[i]->subcategories_[j]->members.cbegin(); 
                    it != categories_[i]->subcategories_[j]->members.cend(); ++it)
          {
            ofs << "            \"" << it->first << "\": " << it->second;
            if (std::next(it) != categories_[i]->subcategories_[j]->members.cend())
            {
              ofs << ",";
            }
            ofs << std::endl;
          }

          ofs << "        }";
          if (j + 1 < categories_[i]->subcategories_.size())
          {
              ofs << ",";
          }
          ofs << std::endl;
        }

        ofs << "    }";
        if (i + 1 < categories_.size())
        {
            ofs << ",";
        }
        ofs << std::endl;
      }
    }

    ofs << "}" << std::endl;
    ofs.close();
  }


} // namespace