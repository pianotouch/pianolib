/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{

  DiscretizedBeam::DiscretizedBeam()
  {
    beam_length = 1.0;
    Nx = 20;
    order = 2;
    Nx_output = 0;
    output_points.Clear();
  }



  void
  DiscretizedBeam
  ::Discretize(const Real_wp &_beam_length, 
                const unsigned &number_elements, 
                const unsigned &_order)
  {
    beam_length = _beam_length;
    order = _order;
    nb_base = order + 1;
    Nx = number_elements;
    ConstructMesh();
    ConstructNumbering();
    ConstructLobattoQuad(order, quad_points, quad_weights);
    ComputeLagrangeCoefs(factors_phi);
    ComputeGradPhi();

    ConstructInterpolationMatrix();
  }



  void
  DiscretizedBeam
  ::ConstructLobattoQuad(const unsigned &r, rvector &pts, 
                                            rvector &wts)
  {
    pts.Reallocate(r+1);
    wts.Reallocate(r+1);

    sym_rmatrix J(r+1, r+1); J.Zero();

    for (unsigned i = 1; i < r; ++i)
    {
      J(i-1, i) = sqrt( i*i / ((2.0*i - 1.0)*(2.0*i + 1.0)) );
    }
    J(r-1, r) = sqrt( r / (2.0*r - 1) );

    rvector lambda_real, permut;
    rmatrix eigen_vectors;

    GetEigenvaluesEigenvectors(J, lambda_real, eigen_vectors);

    Vector<unsigned> idx(r+1); idx.Fill();
    permut = lambda_real; // because Sort(u,v) sorts u, which we do not want
    Sort(permut, idx); // idx is sorted by increasing eigen values

    for (unsigned i = 0; i < r+1; ++i)
    {
      pts(i) = 0.5 + 0.5*lambda_real(idx(i));
      wts(i) = square( eigen_vectors(0,idx(i)) );
    }
    pts(0) = 0.0;
    pts(r) = 1.0;
  }



  void
  DiscretizedBeam
  ::ConstructMesh()
  {
    if (Nx <= 1)
    {
      dx = beam_length;
    }
    else
    {
      dx = beam_length / (Nx - 1);
    }
    
    nodes.Reallocate(Nx);
    for (unsigned i = 0; i < Nx; ++i)
    {
      nodes(i) = dx*i;
    }
  }



  void
  DiscretizedBeam
  ::ConstructNumbering()
  {
    nL2 = (Nx - 1)*(order + 1);
    nH1 = (Nx - 1)*order + 1;
    dof_num_L2.Reallocate(Nx-1, order+1); dof_num_L2.Zero();
    dof_num_H1.Reallocate(Nx-1, order+1); dof_num_H1.Zero();
    
    unsigned num_dof = 0;
    for (unsigned i = 0; i < Nx-1; ++i)
    {
      for (unsigned j = 0; j <= order; ++j)
      {
        dof_num_L2(i, j) = num_dof++;
      }
    }

    num_dof = 0;
    for (unsigned i = 0; i < Nx-1; ++i)
    {
      dof_num_H1(i,0) = num_dof;
      for (unsigned j = 1; j < order; ++j)
      {
        dof_num_H1(i,j) = num_dof+j;
      }
      
      dof_num_H1(i, order) = num_dof + order;
      num_dof += order;
    }
  }



  void
  DiscretizedBeam
  ::ComputeLagrangeCoefs(rvector &lagrange_coefs)
  {
    lagrange_coefs.Reallocate(nb_base);
    for (unsigned i = 0; i < nb_base; ++i)
    {
      lagrange_coefs(i) = 1.0;
      for (unsigned j = 0; j < nb_base; ++j)
      {
        if (j != i)
        {
          lagrange_coefs(i) *= quad_points(i) - quad_points(j);
        }
      }
      
      lagrange_coefs(i) = 1.0/lagrange_coefs(i);
    }
  }



  Real_wp
  DiscretizedBeam
  ::EvaluateBasisFunction(const unsigned &num, const Real_wp &point) const
  {  
    Real_wp temp = 1.0;
    for (unsigned k = 0; k < nb_base; ++k)
    {
      if (k != num)
      {
        temp *= point - quad_points(k);
      }
    }
    
    return temp * factors_phi(num);
  } 



  Real_wp
  DiscretizedBeam
  ::EvaluateGradBasisFunction(const unsigned &num, const Real_wp &point) const
  {
    Real_wp temp = 1.0;
    Real_wp res = 0.0;
    for (unsigned i = 0; i <= order; ++i)
    {
      if (i != num)
      {
        temp = 1;
        for (unsigned j = 0; j <= order; ++j)
        {
          if ((j != num) && (j != i))
          {
            temp *= point - quad_points(j);
          }
        }
        res += temp;
      }
    }
    return res * factors_phi(num);
  }



  void
  DiscretizedBeam
  ::ComputeGradPhi()
  {
    valdPhi.Reallocate(nb_base, nb_base);
    for (unsigned j = 0; j < nb_base; ++j) // Loop on basis functions
    {
      for (unsigned g = 0; g < nb_base; ++g) // Loop on quadrature points
      {
        valdPhi(j,g) = EvaluateGradBasisFunction(j, quad_points(g));
      }
    }
  }




  //! Assemble mass matrix /int M /phi_i /phi_j
  sym_srmatrix
  DiscretizedBeam
  ::AssembleMassMatrix(const rmatrix &M, const bool &use_H1) const
  {
    // M must not have extra diagonal coefs (to fix)
    const size_t size = M.GetM();
    sym_srmatrix mass_matrix;

    if (use_H1)
    {
      mass_matrix.Reallocate(nH1*size, nH1*size);
    }
    else
    {
      mass_matrix.Reallocate(nL2*size, nL2*size);
    }

    unsigned gg;
    for(unsigned ne = 0; ne < Nx-1; ++ne) // Loop on elements
    {
      for (unsigned g = 0; g < nb_base; ++g) // Loop on integration points
      {
        if (use_H1)
        {
          gg = dof_num_H1(ne,g);
        }
        else
        {
          gg = dof_num_L2(ne,g);
        }
        for (unsigned n = 0; n < size; ++n)
        {
          for (unsigned m = 0; m < size; ++m)
          {
            if (M(n,m) != Real_wp(0))
            {
              mass_matrix.AddInteraction(size*gg + n, size*gg + m, 
                                                    dx*M(n,m)*quad_weights(g));
            }
          }
        }
      }
    }
    return mass_matrix;
  }




  //! Assemble semi-grad matrix /int M /phi_i /partial_x/phi_j
  srmatrix
  DiscretizedBeam
  ::AssembleSemiGradMatrix(const rmatrix &M, const bool &use_H1) const
  {
    const size_t size = M.GetM();
    srmatrix fem_matrix;

    if (use_H1)
    {
      fem_matrix.Reallocate(nH1*size, nH1*size);
    }
    else
    {
      fem_matrix.Reallocate(nL2*size, nL2*size);
    }

    unsigned ii, jj;
    Real_wp coef;
    for(unsigned ne = 0; ne < Nx-1; ++ne) // Loop on elements
    {
      for (unsigned i = 0; i < nb_base; ++i) // Loop on test functions
      {
        if (use_H1)
        {
          ii = dof_num_H1(ne,i);
        }
        else
        {
          ii = dof_num_L2(ne,i);
        }
        for (unsigned j = 0; j < nb_base; ++j) // Loop on basis functions
        {
          if (use_H1)
          {
            jj = dof_num_H1(ne,j);
          }
          else
          {
            jj = dof_num_L2(ne,j);
          }
          coef = valdPhi(i,j)*quad_weights(j);
          for (unsigned n = 0; n < size; ++n)
          {
            for (unsigned m = 0; m < size; ++m)
            {
              if (M(n,m) != Real_wp(0))
              {
                fem_matrix.AddInteraction(size*ii + n, size*jj + m, coef*M(n,m));
              }
            }
          }
        }
      }
    }
    return fem_matrix;
  }




  //! Assemble grad-grad matrix /int M /partial_x/phi_i /partial_x/phi_j
  srmatrix
  DiscretizedBeam
  ::AssembleStiffnessMatrix(const rmatrix &M, const bool &use_H1) const
  {
    const size_t size = M.GetM();
    srmatrix stiffness_matrix;

    if (use_H1)
    {
      stiffness_matrix.Reallocate(nH1*size, nH1*size);
    }
    else
    {
      stiffness_matrix.Reallocate(nL2*size, nL2*size);
    }

    unsigned ii, jj;
    Real_wp coef;
    for(unsigned ne = 0; ne < Nx-1; ++ne) // Loop on elements
    {
      for (unsigned i = 0; i < nb_base; ++i) // Loop on test functions
      {
        if (use_H1)
        {
          ii = dof_num_H1(ne,i);
        }
        else
        {
          ii = dof_num_L2(ne,i);
        }
        for (unsigned j = 0; j < nb_base; ++j) // Loop on basis functions
        {
          if (use_H1)
          {
            jj = dof_num_H1(ne,j);
          }
          else
          {
            jj = dof_num_L2(ne,j);
          }
          coef = 0.0;
          for (unsigned g = 0; g < nb_base; ++g) // Loop for quadrature formula
          {
            coef += valdPhi(i,g)*valdPhi(j,g)*quad_weights(g);
          }
          coef = coef / dx;
          for (unsigned n = 0; n < size; ++n)
          {
            for (unsigned m = 0; m < size; ++m)
            {
              if (M(n,m) != Real_wp(0))
              {
                stiffness_matrix.AddInteraction(size*ii + n, size*jj + m, coef*M(n,m));
              }
            }
          }
        }
      }
    }
    return stiffness_matrix;
  }



  void
  DiscretizedBeam
  ::ConstructOutputGrid()
  { 
    if (Nx_output > 0) // If Nx_output is not 0, we construct a regular output grid with Nx_output points
    {
      output_points.Reallocate(Nx_output);
      if (Nx_output == 1)
      {
        output_points(0) = 0.0;
      }
      else
      {
        const Real_wp h = beam_length / (Nx_output - 1);
        for (unsigned i = 0; i < Nx_output; ++i)
        {
          output_points(i) = h*i;
        }
      }
    }
    else if (output_points.GetM() > 0) // If output_points is not empty, we construct the output grid on these points. The grid is not regular in this case.
    {
      Sort(output_points); // sort the points
      Nx_output = output_points.GetM();
    }
    // else if both are 0 we do not want any output.
  }



  void
  DiscretizedBeam
  ::LocalizeOutputPoints()
  {
    output_loc_on_mesh.Reallocate(Nx_output); output_loc_on_mesh.Fill(-1.0);
    output_coord_on_mesh.Reallocate(Nx_output); output_coord_on_mesh.Fill(0.0);
    
    unsigned ne = 0;
    Real_wp xe = nodes(ne);

    const Real_wp Lmax = abs(nodes(Nx-1) - xe);
    for (unsigned i = 0; i < Nx_output; ++i) // loop over points to find
    {
      // we search the next point ne, so that points(i) belongs to (nodes(ne-1), nodes(ne))
      while ((ne < Nx) && (output_points(i) >= xe-100.0*epsilon_machine*Lmax))
      {
        ne++;
        if (ne < Nx)
        {
          xe = nodes(ne);
        }
      }
    
      if ((ne > 0) && (ne < Nx))
      {
        // the point is in the mesh
        output_loc_on_mesh(i) = ne - 1;
        output_coord_on_mesh(i) = (output_points(i) - nodes(ne-1)) / (nodes(ne) - nodes(ne-1));
      }
    
      if (ne == Nx)
      {
        if (abs(output_points(i) - nodes(ne-1)) <= 100.0*epsilon_machine*Lmax)
        {
          output_loc_on_mesh(i) = ne - 2;
          output_coord_on_mesh(i) = 1.0;
        }
        ne++;
      }
    }
  }
  


  void
  DiscretizedBeam
  ::ConstructInterpolationMatrix()
  {
    ConstructOutputGrid();
    LocalizeOutputPoints();

    // interp(j,l) = \phi_j (x_interp_l)
    interp_H1.Reallocate(Nx_output, nH1);
    interp_L2.Reallocate(Nx_output, nL2);

    grad_interp_H1.Reallocate(Nx_output, nH1);
    grad_interp_L2.Reallocate(Nx_output, nL2);

    unsigned ne, jj;
    Real_wp x;
    Real_wp phi, dphi;

    for (unsigned l = 0; l < Nx_output; ++l)
    {
      ne = output_loc_on_mesh(l);
      x = output_coord_on_mesh(l);
      for (unsigned j = 0; j < nb_base; ++j)
      {
        // We put the same values of basis functions
        // but not in the same places depending on H1 or L2
        phi = EvaluateBasisFunction(j,x);
        dphi = EvaluateGradBasisFunction(j,x);
        jj = dof_num_H1(ne,j);
        interp_H1.Get(l, jj) = phi;
        grad_interp_H1.Get(l, jj) = dphi / dx;
        jj = dof_num_L2(ne,j);
        interp_L2.Get(l, jj) = phi;
        grad_interp_L2.Get(l, jj) = dphi / dx;
      }
    }
  }


} // namespace
