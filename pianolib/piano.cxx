/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace pianolib
{


  //! default constructor
  Piano::Piano()
  : timer_ALL(), timer_INIT(), timer_ENERGY(), timer_OUTPUT(), 
    timer_FACTO(), timer_SOLVE(), timer_FILL(), timer_DEBUG()
  {
    duration = 0.02;
    dt = -1;
    cfl_fraction = 10;

    current_time = 0.0;
    current_iter = 0;

    remove_hammer = false;
    hammer_is_removed = false;
    remove_hammer_threshold = 0.01;

    contact_start = false;
    contact_stop = false;
    start_time_contact = 0.0;
    contact_duration = 0.0;
    speed_contact_trans = 0.0;
    speed_contact_longi = 0.0;


    file_base = "./";
    interp_grad = false;
    output_on_ddls = false;
    output_on_nodes = false;
    compute_energy = false;
    output_aux_var = false;
    dt_outputs = 1e-5;
    size_buffer = 100;

    nan_detected = false;
  }



  //! modification des parametres du marteau avec une ligne du fichier de donnees
  /*!
    \param[in] description_field mot-cle de la ligne (par exemple Hammer = ...)
    \param[in] parameters liste des parametres associes au mot-cle
  */
  void
  Piano
  ::SetInputData(const std::string &keyword, const Vector<std::string> &param)
  {
    if (keyword == "TimeStep")
    {
      if (param(0) == "AUTO")
      {
        dt = -1;
        if (param.GetM() >= 2)
        {
          cfl_fraction = to_num<Real_wp>(param(1));
        }
      }
      else
      {
        dt = to_num<Real_wp>(param(0));
        shank.dt = dt;
        pstring.dt = dt;
      }
    }
    else if (keyword == "SimulationDuration")
    {
      duration = to_num<Real_wp>(param(0));
      shank.duration = duration;
      pstring.duration = duration;
    }
    else if (keyword == "DirectoryOutput")
    {
      file_base = param(0);
      shank.file_base = file_base;
      pstring.file_base = file_base;
    }
    else if (keyword == "TimeStepOutputs")
    {
      if (param.GetM() != 1)
      {
        std::cout << "In SetInputData of Piano" << std::endl;
        std::cout << "TimeStepOutputs needs one parameters, for instance :" << std::endl;
        std::cout << "TimeStepOutputs = 1e-5" << std::endl;
        std::cout << "Current parameters are : " << std::endl << param << std::endl;
        abort();
      }
      dt_outputs = to_num<Real_wp>(param(0));
      shank.dt_outputs = dt_outputs;
      pstring.dt_outputs = dt_outputs;
    }
    else if (keyword == "OutputEnergy")
    {
      if (param.GetM() != 1)
      {
        std::cout << "In SetInputData of Piano" << std::endl;
        std::cout << "OutputEnergy needs one parameters, for instance :" << std::endl;
        std::cout << "OutputEnergy = YES or NO" << std::endl;
        std::cout << "Current parameters are : " << std::endl << param << std::endl;
        abort();
      }
      if (param(0) == "YES")
      {
        compute_energy = true;
        pstring.compute_energy = true;
        shank.compute_energy = true;
      }
    }
    else if (keyword == "OutputAuxVar")
    {
      if (param.GetM() != 1)
      {
        std::cout << "In SetInputData of Piano" << std::endl;
        std::cout << "OutputAuxVar needs one parameters, for instance :" << std::endl;
        std::cout << "OutputAuxVar = YES or NO" << std::endl;
        std::cout << "Current parameters are : " << std::endl << param << std::endl;
        abort();
      }
      if (param(0) == "YES")
      {
        output_aux_var = true;
        shank.output_aux_var = output_aux_var;
        pstring.output_aux_var = output_aux_var;
      }
    }
    else if (keyword == "OutputGradient")
    {
      if (param.GetM() != 1)
      {
        std::cout << "In SetInputData of Piano" << std::endl;
        std::cout << "OutputGradient needs one parameters, for instance :" << std::endl;
        std::cout << "OutputGradient = YES or NO" << std::endl;
        std::cout << "Current parameters are : " << std::endl << param << std::endl;
        abort();
      }
      if (param(0) == "YES")
      {
        interp_grad = true;
        shank.interp_grad = interp_grad;
        pstring.interp_grad = interp_grad;
      }
    }
    else if (keyword == "SizeBuffer")
    {
      size_buffer = to_num<Real_wp>(param(0));
      shank.size_buffer = size_buffer;
      pstring.size_buffer = size_buffer;
      //soundboard.size_buffer = size_buffer;
    }

    else if (keyword == "RemoveHammerAfterFirstStrike")
    {
      if (param.GetM() > 2)
      {
        std::cout << "In SetInputData of Piano" << std::endl;
        std::cout << "RemoveHammerAfterFirstStrike needs one or two parameters, for instance :" << std::endl;
        std::cout << "RemoveHammerAfterFirstStrike = YES 0.01" << std::endl;
        std::cout << "RemoveHammerAfterFirstStrike = YES" << std::endl;
        std::cout << "RemoveHammerAfterFirstStrike = NO" << std::endl;
        std::cout << "Current parameters are : " << std::endl << param << std::endl;
        abort();
      }
      if (param(0) == "YES")
      {
        remove_hammer = true;
        if (param.GetM() == 2)
        {
          remove_hammer_threshold = to_num<Real_wp>(param(1));
        }
      }
    }

    pstring.SetInputData(keyword, param);
    shank.SetInputData(keyword, param);

  }



  void
  Piano
  ::DisplayInit(std::ostream &out) const
  {
    out << " *------------------------------------*" << std::endl;
    out << " *         GENERAL INFOMATION         *" << std::endl;
    out << " *-----------------------------*------*" << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Machine Epsilon             | " << epsilon_machine << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << "" << std::endl;
    out << " *------------------------------------*" << std::endl;
    out << " *         TIME DISCRETISATION        *" << std::endl;
    out << " *-----------------------------*------*" << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Duration (s)                | " << duration << std::endl;
    out.precision(20);
    out << " | Time Step (s)               | " << dt << std::endl;
    out << " | Time Step Output (s)        | " << dt_outputs << std::endl;
    out.precision(15);
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << "" << std::endl;
    out << "" << std::endl;

#ifndef SHANK
    pstring.DisplayInit(out);
#endif
    shank.DisplayInit(out);
  }



  void
  Piano
  ::WriteSummaryBegin(JsonManager* jsm) const
  {
    jsm->AddMember("Machine Epsilon", to_str(epsilon_machine));
    jsm->AddMember("Simulation duration", to_str(duration));
    jsm->AddMember("Time step", to_str(dt));
    jsm->AddMember("Time step output", to_str(dt_outputs));

#ifndef SHANK
    pstring.WriteJsonSummary(jsm);
#endif
    shank.WriteJsonSummary(jsm);
  }



  void
  Piano
  ::DisplaySimuAdvance()
  {
    //printf("\033[%d;%dH", YPos+1, XPos+1); // Set cursor position
    if (current_iter != 0)
    {
      printf("\033[%dA", 3); // Moves cursor up 3 lines
    }
    timer_ALL.Stop();
    const double time_all = timer_ALL.GetSeconds() - timer_INIT.GetSeconds();
    timer_ALL.Start();

    const double tot_time = time_all * nb_iter / double(current_iter+1);
    double hours = int(tot_time / 3600);
    double minutes = int((tot_time - hours*3600) / 60);
    double seconds = ceil((tot_time - hours*3600 - minutes*60));
    std::cout << "Estimated duration: "
         << hours << " Hours, "
         << minutes << " Minutes, "
         << seconds << " Seconds."
         << std::endl;
    const double remain_time = tot_time - time_all;
    hours = int(remain_time / 3600);
    minutes = int((remain_time - hours*3600) / 60);
    seconds = ceil((remain_time - hours*3600 - minutes*60));
    std::cout << "Remaining: "
         << hours << " Hours, "
         << minutes << " Minutes, "
         << seconds << " Seconds."
         << std::endl;
    std::cout << "" << std::endl;


    const Real_wp progress = Real_wp(current_iter) / Real_wp(nb_iter);

    const unsigned barWidth = 70;

    std::cout << "[";
    const unsigned pos = barWidth * progress;
    for (unsigned i = 0; i < barWidth; ++i)
    {
      if (nan_detected)
      {
        if ((i < pos) && (i < 20)) std::cout << "=";
        else if ((i < pos) && (i > 20) && (i < 44)) std::cout << "";
        else if ((i < pos) && (i > 20) && (i >= 44)) std::cout << "=";
        else if (i == 20) std::cout << " \033[31mWARNING: NaN detected!\033[0m ";
        else if ((i == pos) && (i < 20)) std::cout << ">";
        else if ((i == pos) && (i > 20) && (i < 44)) std::cout << "";
        else if ((i == pos) && (i > 20) && (i >= 44)) std::cout << ">";
        else if (i < 20) std::cout << " ";
        else if ((i > 20) && (i < 44)) std::cout << "";
        else if ((i > 20) && (i >= 44)) std::cout << " ";
      }
      else
      {
        if (i < pos) std::cout << "=";
        else if (i == pos) std::cout << ">";
        else std::cout << " ";
      }
    }
    std::cout << "] " << int(progress * 100.0) << " %\r";
    std::cout.flush();
  }




  void
  Piano
  ::DisplayFinalize(std::ostream &out) const
  {
    // https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
    // for display and color options in terminal
    if (nan_detected)
    {
      out << "[================= \033[31mSIMULATION COMPLETED BUT DIVERGED\033[0m";
      out << " =================>] 100 %" << std::endl;
    }
    else
    {
      out << "[======================= \033[32mSIMULATION COMPLETED\033[0m";
      out << " ========================>] 100 %" << std::endl;
    }

    const double time_all = timer_ALL.GetSeconds();

    out << "" << std::endl;
    out.precision(4);
    out << "" << std::endl;
    out << " *------------------------------------*" << std::endl;
    out << " *          COMPUTATION TIMES         *" << std::endl;
    out << " *-----------------------------*------*" << std::endl;
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << " | Total                       | " << time_all << " seconds" << std::endl;
    out << " | Init                        | "
         << 100*timer_INIT.GetSeconds()/time_all << " %" << std::endl;
    out << " | Output                      | "
         << 100*timer_OUTPUT.GetSeconds()/time_all << " %" << std::endl;
    if (compute_energy)
    {
      out << " | Energy                      | "
           << 100*timer_ENERGY.GetSeconds()/time_all << " %" << std::endl;
    }
    out << " | Factorization               | "
         << 100*timer_FACTO.GetSeconds()/time_all << " %" << std::endl;
    out << " | Solve                       | "
         << 100*timer_SOLVE.GetSeconds()/time_all << " %" << std::endl;
    out << " | Fill the Jacobian           | "
         << 100*timer_FILL.GetSeconds()/time_all << " %" << std::endl;
    out << " | Boundary conditions         | "
         << 100*timer_BOUND.GetSeconds()/time_all << " %" << std::endl;
    
    out << " +-----------------------------+------------------------+ " << std::endl;
    out << "" << std::endl;
    out << "" << std::endl;
    out.precision(15);
  }



  void
  Piano
  ::WriteSummaryEnd(JsonManager* jsm) const
  {
    jsm->AddMember("Computation time", to_str(timer_ALL.GetSeconds()));
  }



  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  //    MAIN PROGRAMS
  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-


  void
  Piano
  ::Init()
  {
    InitBuffer();
    shank.Init();

#ifndef SHANK
    pstring.Init();

    LM_sndb.Reallocate(pstring.model->nb_LM); LM_sndb.Zero();
    LM_sndb_plus.Reallocate(pstring.model->nb_LM); LM_sndb_plus.Zero();

    ComputeRepartitionHammerString();
#endif

    if (dt < 0) // dt is negative if TimeStep was specified AUTO in the ini file
    { // In this situation dt is determined automatically with the CFL
      ChooseTimeStepCFL();
    }

    nb_iter = unsigned(ceil(duration/dt));
    shank.nb_iter = nb_iter;
    pstring.nb_iter = nb_iter;
    ConstructTimeInterp();

    ComputeSourceTerms();

    shape = shank.shape + 9; // lagrange mult (1) + XY head position and speed (4) + quadratization variable (2) + Fcoupl (1) + Fstring (1)
    shank.offset = 0;
#ifndef SHANK
    shape += pstring.shape + 1 + pstring.model->nb_LM; // quadratization variable (1) + lagrange mult at x=L (nb_LM)
    pstring.offset = shank.shape + 9;
#endif

    ConstDiffMatrix.Reallocate(shape, shape);
    shank.ComputeConstDiffMatrix(ConstDiffMatrix);

#ifndef SHANK
    pstring.ComputeConstDiffMatrix(ConstDiffMatrix);
    ComputeConstDiffMatrixBridgeCoupling(ConstDiffMatrix);
    ComputeConstDiffMatrixHammerStringCoupling(ConstDiffMatrix);
#endif
  }



  void
  Piano
  ::ChooseTimeStepCFL()
  {
#ifdef SHANK
    shank.ChooseTimeStepCFL(dt);
#endif
#ifdef STRING
    pstring.ChooseTimeStepCFL(dt);
#endif
#ifdef PIANO
    std::cout << "No CFL available for full piano." << std::endl;
    std::cout << "Please choose a non-AUTO time step." << std::endl;
    abort();
#endif
  }



  void
  Piano
  ::ApplyBoundaryConditions(srmatrix &M) const
  {
    shank.ApplyBoundaryConditions(M);
#ifndef SHANK
    pstring.ApplyBoundaryConditions(M);
#endif
  }



  void
  Piano
  ::OneStep()
  {
    timer_FILL.Start();
    DiffMatrix.Reallocate(shape, shape);
    RHS.Reallocate(shape); RHS.Zero();

#ifndef SHANK
    pstring.OneStep(DiffMatrix, RHS);
#endif

    if (!hammer_is_removed)
    {
      shank.OneStep(DiffMatrix, RHS);
      ComputeDiffMatrixHammerStringCoupling(DiffMatrix);
    }
    Add(Real_wp(1.0), ConstDiffMatrix, DiffMatrix);

    timer_BOUND.Start();
    ApplyBoundaryConditions(DiffMatrix);
    timer_BOUND.Stop();
    timer_FILL.Stop();

    /*DiffMatrix.WriteText("DiffMatrix.txt");
    ConstDiffMatrix.WriteText("ConstDiffMatrix.txt");
    abort();*/

    timer_FACTO.Start();
    solver.Factorize(DiffMatrix); // Heavy computations here!
    timer_FACTO.Stop();

    timer_SOLVE.Start();
    solver.Solve(RHS); // RHS is the new solution
    timer_SOLVE.Stop();

#ifndef SHANK
    pstring.ExtractNewSol(RHS);
    for (unsigned k = 0; k < pstring.model->nb_LM; ++k) //bridge.ExtractNewSol()
    {
      LM_sndb_plus(k) = RHS(pstring.offset+pstring.shape+1+k);
    }
#endif
    if (!hammer_is_removed)
    {
      shank.ExtractNewSol(RHS);
    }

  }



  void
  Piano
  ::RunSimulation(JsonManager* jsm)
  {
    timer_ALL.Start();

    timer_INIT.Start();
    Init();
    DisplayInit(std::cout);
    WriteSummaryBegin(jsm);
    OutputFemMatrices();
    timer_INIT.Stop();

    if (compute_energy)
    {
      timer_ENERGY.Start();
      current_time = -dt;
      pstring.current_time = current_time;
      shank.current_time = current_time;
      WriteEnergy();
      timer_ENERGY.Stop();
    }

    for (unsigned iter = 0; iter < nb_iter+1; ++iter)
    {
      current_time = iter*dt;
      current_iter = iter;

      pstring.current_time = current_time;
      shank.current_time = current_time;
      pstring.current_iter = current_iter;
      shank.current_iter = current_iter;

      OneStep(); // Major computations

      if (compute_energy)
      {
        timer_ENERGY.Start();
        WriteEnergy();
        timer_ENERGY.Stop();
      }

      timer_OUTPUT.Start();
      WriteSismos();
      timer_OUTPUT.Stop();

      UpdateValues();

      CheckHammerStuff();

      if (!nan_detected)
      {
        if (std::isnan(shank.Q(shank.shank.dimension)))
        {
          nan_detected = true;
        }
      }

      if (iter % unsigned(nb_iter/std::min<unsigned>(70,nb_iter)) == 0)
      { // The display is updated every 70 iterations
        DisplaySimuAdvance();
      }
    }

    timer_ALL.Stop();

    DisplayFinalize(std::cout);
    WriteSummaryEnd(jsm);
  }



  



  void
  Piano
  ::UpdateValues()
  {
    if (!hammer_is_removed)
    {
      shank.UpdateValues();
    }
#ifndef SHANK
    pstring.UpdateValues();
    LM_sndb = LM_sndb_plus;
#endif
  }



  void
  Piano
  ::OutputFemMatrices()
  {
#ifndef SHANK
    if (pstring.output_fem_matrices)
      pstring.OutputFemMatrices(file_base);
#endif
    if (shank.output_fem_matrices)
      shank.OutputFemMatrices(file_base);
  }




  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-
  //    OUTPUTS
  //*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-



  void
  Piano
  ::ConstructTimeInterp()
  {
    grid_interp_time.Nx_output = unsigned(ceil(duration/dt_outputs));
    const Real_wp exact_duration = nb_iter*dt;
    grid_interp_time.Discretize(exact_duration, nb_iter+1, 1);
  }



  void
  Piano
  ::ComputeSourceTerms()
  {
    shank.ComputeSourceTerms();
  }



  void
  Piano
  ::InitBuffer()
  {
#ifndef SHANK
    output_LM_sndb.SetDoublePrecision();
    output_LM_sndb.Init(file_base + file_output_LM_sndb + ".txt",
                                    size_buffer);
#endif
    if (compute_energy)
    {
      output_energy.SetDoublePrecision();
      output_energy.Init(file_base + file_output_energy + ".txt",
                          size_buffer);
    }
  }



  void
  Piano
  ::WriteSismos()
  {
    if (!hammer_is_removed)
    {
      shank.WriteSismos(grid_interp_time);
    }
#ifndef SHANK
    pstring.WriteSismos(grid_interp_time);

    for (unsigned l = 0; l < grid_interp_time.Nx_output; ++l)
    {
      const unsigned ne_loc = grid_interp_time.output_loc_on_mesh(l);
      if (ne_loc == current_iter)
      {
        const Real_wp t = grid_interp_time.output_coord_on_mesh(l);
        const Real_wp current_time_output = dt_outputs*l;

        rvector temp_LM_sndb;
        temp_LM_sndb.Reallocate(pstring.model->nb_LM+1);
        temp_LM_sndb(0) = current_time_output;
        for (unsigned k = 0; k < pstring.model->nb_LM; ++k)
        {
          temp_LM_sndb(k+1) = LM_sndb(k)*(1.0-t) + LM_sndb_plus(k)*t;
        }
        output_LM_sndb.AddVect(temp_LM_sndb);
      }
    }
#endif
  }



  void
  Piano
  ::WriteEnergy()
  {
    rvector temp(6); temp.Zero();
    temp(0) = current_time+dt;

    temp(1) += shank.ComputeKineticEnergy();
    temp(2) += shank.ComputePotentialEnergy();
    temp(3) += shank.ComputeNonLinearEnergy();

    temp(5) += shank.ComputeDissipatedEnergy()
                + ComputeDissipatedEnergyHammerStringCoupling();

#ifndef SHANK

    temp(1) += pstring.ComputeKineticEnergy();
    temp(2) += pstring.ComputePotentialEnergy();
    temp(3) += pstring.ComputeNonLinearEnergy();

    temp(5) += pstring.ComputeDissipatedEnergy();

#endif

    temp(4) = temp(1) + temp(2) + temp(3);

    output_energy.AddVect(temp);
  }


} // namespace