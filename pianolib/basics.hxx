/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

namespace Seldon
{ // Definition of the machine precision
  #ifdef FLOAT
    typedef float Real_wp;
    typedef std::complex<float> Complex_wp;
  #endif
  #ifdef DOUBLE
    typedef double Real_wp;
    typedef std::complex<double> Complex_wp;
  #endif
}

namespace pianolib
{

  const Real_wp pi = M_PI;
  const Real_wp gravity = 9.81; // Gravity constant on earth
  const Real_wp sound_speed = 343.0; // At 20°C
  const Real_wp air_density = 1.204; // At 20°C
  const Real_wp epsilon_machine = std::numeric_limits<Real_wp>::epsilon();
  const Complex_wp Iwp(0.0, 1.0);

  
  typedef Vector<Real_wp> rvector;
  typedef Vector<Complex_wp> cvector;
  typedef Vector<Real_wp, VectSparse> srvector;

  typedef Matrix<Real_wp> rmatrix;
  typedef Matrix<Real_wp, Symmetric, RowSymPacked> sym_rmatrix;
  typedef Matrix<Real_wp, General, ArrayRowSparse> srmatrix;
  typedef Matrix<Real_wp, Symmetric, ArrayRowSymSparse> sym_srmatrix;



  inline void InitPianolib()
  {
    std::cout.precision(15);
    std::cerr.precision(15);
  };


} // namespace