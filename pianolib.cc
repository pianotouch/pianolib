/* 
 * Copyright (C) 2024, Guillaume Castera, Juliette Chabassier
 *
 * This file is part of Pianolib.
 *
 * Pianolib is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pianolib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// Seldon is used for fast linear algebra
#include "SeldonLib.hxx"
using namespace Seldon;

// Some more useful things
#include <numeric>
#include <chrono>

// And pianolib stuff
#include "pianolib/pianolib.hh"




using namespace pianolib;


int main(int argc, char** argv)
{

  if (argc == 1)
  {
  	std::cout << "This code requires a file with parameters." << std::endl;
  	std::cout << "Please run ./some_rule.x param_file.ini" << std::endl;
  	abort();
  }

  InitPianolib();

  {
    Piano piano; // Declare Piano object containing everything

    std::string name_data_file(argv[1]);
    ReadInputFile(name_data_file, piano); // Read the .ini file

    std::string file_name = piano.file_base + piano.file_summary;
    JsonManager* jsm = new JsonManager(file_name);

    piano.RunSimulation(jsm); // Run the time iterations

    jsm->Write();
  }

  return 0;
}
