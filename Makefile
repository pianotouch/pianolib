#*****************************************************************************#
#                                                                             #
#                   ______                      __  __                        #
#                  /  __  \__ ____  ___  ____  / /_/ /_                       #
#                 /  / /  / / __  / __ \/ __ \/ / / __ \                      #
#                /  /_/  / / __  / / / / /_/ / / / /_/ /                      #
#               /  .____/_/_/ /_/_/ /_/\____/_/_/_____/                       #
#              /  /                                                           #
#             /__/   for piano numerical simulation                           #
#                                                                             #
#                                                                             #
#*****************************************************************************#

#-----------------------------------------------------------------------------
# C++ compiler
#-----------------------------------------------------------------------------

# Pianolib has been tested, and should compile correctly, with:
# - Linux: g++
# - Mac OS: g++, clang++, g++-11, g++-12, g++-13, g++-14
CXX = g++
CXXFLAGS = -std=c++20



#-----------------------------------------------------------------------------
# Optimization and debug options
#-----------------------------------------------------------------------------

# Degree of optimization
# - DEGUB for extra prints, no optimization, and seldon internal tests
# - FAST (or anything else) for optimized code
OPTIMIZATION = FAST

ifeq ($(OPTIMIZATION),DEBUG)
	OPTIM = -O0 -DDEBUG -DSELDON_DEBUG_LEVEL_4 -g
	CXXFLAGS := $(CXXFLAGS) -Wall -Wextra -Wno-strict-overflow -Wno-unused-parameter
else
	OPTIM = -O3 -DSELDON_DEBUG_LEVEL_1 -DNDEBUG
endif



#-----------------------------------------------------------------------------
# Precision of the real numbers
#-----------------------------------------------------------------------------

# FLOAT or DOUBLE
PRECISION = DOUBLE
FLAGS := $(FLAGS) -D$(PRECISION)



#-----------------------------------------------------------------------------
# External libraries and includes
#-----------------------------------------------------------------------------

EXTERNAL := ./external
SELDON_PATH := $(EXTERNAL)/SELDON
LAPACK_PATH := $(EXTERNAL)/LAPACK/build/lib
METIS_PATH  := $(EXTERNAL)/METIS
ARPACK_PATH := $(EXTERNAL)/ARPACK

# Metis does not always improves the efficiency, try with or without...
USE_METIS = NO
# Arpack is mandatory for CFL computations
USE_ARPACK = NO

FLAGS := $(FLAGS) -DSELDON_WITH_LAPACK -DSELDON_WITH_CBLAS
INCLUDE := $(INCLUDE) -I$(SELDON_PATH)
LIB := $(LIB) -L$(LAPACK_PATH) -lcblas -llapack -lblas

ifeq ($(USE_METIS), YES)
	LIB := $(LIB) -L$(METIS_PATH) -lmetis
	INCLUDE := $(INCLUDE) -I$(METIS_PATH)/include
endif

ifeq ($(USE_ARPACK), YES)
	FLAGS := $(FLAGS) -DSELDON_WITH_ARPACK
	LIB := $(LIB) -L$(ARPACK_PATH) -larpack

	OS_NAME := $(shell uname -s)
	ifeq ($(OS_NAME), Linux)
		LIB := $(LIB) -lgfortran
	else ifeq ($(OS_NAME), Darwin)
		MACOS_ARCH := $(shell uname -p)
		ifeq ($(MACOS_ARCH), arm)
			LIB := $(LIB) -L/opt/homebrew/lib/gcc/current/ -lgfortran
		else ifeq ($(MACOS_ARCH), x86_64)
			LIB := $(LIB) -L/usr/local/lib/gcc/current/ -lgfortran
		else ifeq ($(MACOS_ARCH), i386)
			LIB := $(LIB) -L/usr/local/lib/gcc/current/ -lgfortran
		else
			$(error Unrecognized Mac OS processor architecture.)
		endif
	endif
endif


#-----------------------------------------------------------------------------
# Compilation rules
#-----------------------------------------------------------------------------

all: messages piano shank
	@echo
	@echo "# ************************************************** #"
	@echo "#                                                    #"
	@echo "#          All rules successfully compiled!          #"
	@echo "#                                                    #"
	@echo "# ************************************************** #"
	@echo

messages:
	@echo
	@echo "# ************* Compilation of Pianolib ************* #"
	@echo "#                                                     #"
	@echo "# Using external libraries:                           #"
	@echo "# -------------------------                           #"
	@echo "#   - Seldon                                          #"
	@echo "#   - Blas, CBlas and Lapack                          #"
ifeq ($(USE_METIS), YES)
	@echo "#   - Metis                                           #"
endif
ifeq ($(USE_ARPACK), YES)
	@echo "#   - Arpack                                          #"
endif
	@echo "#                                                     #"
	@echo "# *************************************************** #"

piano: messages
	@echo
	@echo "Piano rule:"
	@echo "-----------"
	$(CXX) $(CXXFLAGS) $(FLAGS) $(OPTIM) $(INCLUDE) -DPIANO pianolib.cc $(LIB) -o $@.x
	@echo
	@echo "*****************"
	@echo "*    Success    *"
	@echo "*****************"

shank: messages
	@echo
	@echo "Shank rule:"
	@echo "-----------"
	$(CXX) $(CXXFLAGS) $(FLAGS) $(OPTIM) $(INCLUDE) -DSHANK pianolib.cc $(LIB) -o $@.x
	@echo
	@echo "*****************"
	@echo "*    Success    *"
	@echo "*****************"

clean:
	@rm -f *.o *.x


