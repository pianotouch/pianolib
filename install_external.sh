#!/bin/bash

exit_with_error() {
    echo
    echo "Something went wrong during installation of the external dependencies."
    echo "Please report this issue if it persists."
    exit 1
}

set -e
trap 'exit_with_error' ERR

if [[ "$(uname -s)" == "Linux" ]]; then
    NB_PROC=$(nproc)
elif [[ "$(uname -s)" == "Darwin" ]]; then
    NB_PROC=$(sysctl -n hw.ncpu)
else
    NB_PROC=1
fi


rm -rf external
mkdir external
cd external


echo
echo "*****************"
echo "*    SELDON     *"
echo "*****************"
echo
wget https://www.math.u-bordeaux.fr/~durufle/seldon.tar.bz2
tar jxvf seldon.tar.bz2
rm seldon.tar.bz2
cd SELDON
rm version
lines_to_comment="34,35,36,37,41"
for line_number in $(echo $lines_to_comment | tr ',' '\n')
do
    sed -i -e "${line_number}s/^/\/\/ /" SeldonFlag.hxx
done
cd ..

echo
echo "*****************"
echo "*    LAPACK     *"
echo "*****************"
echo
mkdir LAPACK
cd LAPACK
wget https://github.com/Reference-LAPACK/lapack/archive/refs/tags/v3.12.0.tar.gz
tar zxvf v3.12.0.tar.gz
rm v3.12.0.tar.gz
mv lapack-3.12.0 lapack
cd lapack
sed -i -e 's|option(CBLAS "Build CBLAS" OFF)|option(CBLAS "Build CBLAS" ON)|g' CMakeLists.txt
cd ..
mkdir build
cd build
cmake ../lapack
make -j$NB_PROC
cd ../..

echo
echo "*****************"
echo "*     METIS     *"
echo "*****************"
echo
wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz
tar zxvf metis-5.1.0.tar.gz
mv metis-5.1.0 METIS
rm metis-5.1.0.tar.gz
cd METIS
make config
make -j$NB_PROC
mv build/*/libmetis/libmetis.a .
cd ..

echo
echo "*****************"
echo "*    ARPACK     *"
echo "*****************"
echo
wget https://bitbucket.org/chaoyang2013/arpack/get/4b070f981380.zip
unzip 4b070f981380.zip
mv chaoyang2013-arpack-4b070f981380 ARPACK
rm 4b070f981380.zip
cd ARPACK
current_folder=$(pwd)
sed -i -e "s|home          = /global/cscratch1/sd/cyang/ARPACK|home = $current_folder|g" ARmake.inc
sed -i -e 's|PLAT          = Cori|PLAT          =|g' ARmake.inc
sed -i -e 's|ARPACKLIB  = $(home)/libarpack_$(PLAT).a|ARPACKLIB  = $(home)/libarpack.a|g' ARmake.inc
sed -i -e 's|FC      = ftn|FC      = mpif90 -fallow-argument-mismatch|g' ARmake.inc
sed -i -e 's|PFC      = ftn|PFC     = mpif90 -fallow-argument-mismatch|g' ARmake.inc
make lib
cd ..


cd ..
### Back in pianolib

echo
echo "# ************************************************** #"
echo "#                                                    #"
echo "#  All external dependencies successfully installed  #"
echo "#                                                    #"
echo "# ************************************************** #"
