# Changelog

All significant changes to this project will be documented in this file.



## 0.1.2 - 2024-08-01

### Added

- MacOs native compilers (g++ and clang++) are now supported



## 0.1.1 - 2024-05-28 

### Fixed

- Compilation, especially for linux


### Updated

- An other version of arpack is used to allow linux compilation


### Changed

- The summary file with all the parameters of the simulation is now a .json instead of a .txt



## 0.1.0 - 2024-05-07

Initial release. Allows the simulation of the hammer with flexible shank impacting a rigid obstacle or a string.